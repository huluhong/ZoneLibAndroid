package com.efounder.zone.bean;

import java.io.Serializable;
import java.util.List;

public class TalksBean implements Serializable{

    /**
     * obj : {"transmitSourceAvatar":null,"likeCount":1,"readCount":38,"commentStatus":0,"transmitContent":null,"transmitSourceUserid":null,"postCreatetime":"1525311791000","postContent":"5515","commentCount":2,"transmitSourceId":null,"id":2854,"transmitStatus":0,"postExcerpt":null,"device":"FRD-AL10","postUpdatetime":"1525311791000","postStatus":0,"transmitSourceUsername":null,"postAuthor":2920}
     * objType : 2
     * comment : [{"id":3139,"createtime":"1525311898000","content":"textimages","parentId":null,"childComments":null,"commentUserAvatar":null,"objectId":2854,"commentUserNickname":null,"replyUserId":null,"replyUserAvatar":null,"commentUserId":2369,"objectType":2,"replyUserNickname":null},{"id":3140,"createtime":"1525311902000","content":"text456images:","parentId":null,"childComments":null,"commentUserAvatar":null,"objectId":2854,"commentUserNickname":null,"replyUserId":null,"replyUserAvatar":null,"commentUserId":2369,"objectType":2,"replyUserNickname":null}]
     * likes : [{"id":2187,"createtime":"1525311894000","avatar":null,"objectType":2,"nickName":null,"objectId":2854,"userId":2369}]
     */

    private ObjBean obj;
    private int objType;
    private List<CommentBean> comment;
    private List<LikesBean> likes;

    public ObjBean getObj() {
        return obj;
    }

    public void setObj(ObjBean obj) {
        this.obj = obj;
    }

    public int getObjType() {
        return objType;
    }

    public void setObjType(int objType) {
        this.objType = objType;
    }

    public List<CommentBean> getComment() {
        return comment;
    }

    public void setComment(List<CommentBean> comment) {
        this.comment = comment;
    }

    public List<LikesBean> getLikes() {
        return likes;
    }

    public void setLikes(List<LikesBean> likes) {
        this.likes = likes;
    }

    public static class ObjBean implements Serializable{
        /**
         * transmitSourceAvatar : null
         * likeCount : 1
         * readCount : 38
         * commentStatus : 0
         * transmitContent : null
         * transmitSourceUserid : null
         * postCreatetime : 1525311791000
         * postContent : 5515
         * commentCount : 2
         * transmitSourceId : null
         * id : 2854
         * transmitStatus : 0
         * postExcerpt : null
         * device : FRD-AL10
         * postUpdatetime : 1525311791000
         * postStatus : 0
         * transmitSourceUsername : null
         * postAuthor : 2920
         */

        private Object transmitSourceAvatar;
        private int likeCount;
        private int readCount;
        private int commentStatus;
        private Object transmitContent;
        private Object transmitSourceUserid;
        private String postCreatetime;
        private String postContent;
        private int commentCount;
        private Object transmitSourceId;
        private int id;
        private int transmitStatus;
        private Object postExcerpt;
        private String device;
        private String postUpdatetime;
        private int postStatus;
        private Object transmitSourceUsername;
        private int postAuthor;
        private int integral ;

        private String  authorUserName;
        private String  authorAvatar;

        public String getAuthorUserName() {
            return authorUserName;
        }

        public ObjBean setAuthorUserName(String authorUserName) {
            this.authorUserName = authorUserName;
            return this;
        }

        public String getAuthorAvatar() {
            return authorAvatar;
        }

        public ObjBean setAuthorAvatar(String authorAvatar) {
            this.authorAvatar = authorAvatar;
            return this;
        }

        public int getIntegral() {
            return integral;
        }

        public ObjBean setIntegral(int integral) {
            this.integral = integral;
            return this;
        }

        public Object getTransmitSourceAvatar() {
            return transmitSourceAvatar;
        }

        public void setTransmitSourceAvatar(Object transmitSourceAvatar) {
            this.transmitSourceAvatar = transmitSourceAvatar;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(int likeCount) {
            this.likeCount = likeCount;
        }

        public int getReadCount() {
            return readCount;
        }

        public void setReadCount(int readCount) {
            this.readCount = readCount;
        }

        public int getCommentStatus() {
            return commentStatus;
        }

        public void setCommentStatus(int commentStatus) {
            this.commentStatus = commentStatus;
        }

        public Object getTransmitContent() {
            return transmitContent;
        }

        public void setTransmitContent(Object transmitContent) {
            this.transmitContent = transmitContent;
        }

        public Object getTransmitSourceUserid() {
            return transmitSourceUserid;
        }

        public void setTransmitSourceUserid(Object transmitSourceUserid) {
            this.transmitSourceUserid = transmitSourceUserid;
        }

        public String getPostCreatetime() {
            return postCreatetime;
        }

        public void setPostCreatetime(String postCreatetime) {
            this.postCreatetime = postCreatetime;
        }

        public String getPostContent() {
            return postContent;
        }

        public void setPostContent(String postContent) {
            this.postContent = postContent;
        }

        public int getCommentCount() {
            return commentCount;
        }

        public void setCommentCount(int commentCount) {
            this.commentCount = commentCount;
        }

        public Object getTransmitSourceId() {
            return transmitSourceId;
        }

        public void setTransmitSourceId(Object transmitSourceId) {
            this.transmitSourceId = transmitSourceId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getTransmitStatus() {
            return transmitStatus;
        }

        public void setTransmitStatus(int transmitStatus) {
            this.transmitStatus = transmitStatus;
        }

        public Object getPostExcerpt() {
            return postExcerpt;
        }

        public void setPostExcerpt(Object postExcerpt) {
            this.postExcerpt = postExcerpt;
        }

        public String getDevice() {
            return device;
        }

        public void setDevice(String device) {
            this.device = device;
        }

        public String getPostUpdatetime() {
            return postUpdatetime;
        }

        public void setPostUpdatetime(String postUpdatetime) {
            this.postUpdatetime = postUpdatetime;
        }

        public int getPostStatus() {
            return postStatus;
        }

        public void setPostStatus(int postStatus) {
            this.postStatus = postStatus;
        }

        public Object getTransmitSourceUsername() {
            return transmitSourceUsername;
        }

        public void setTransmitSourceUsername(Object transmitSourceUsername) {
            this.transmitSourceUsername = transmitSourceUsername;
        }

        public int getPostAuthor() {
            return postAuthor;
        }

        public void setPostAuthor(int postAuthor) {
            this.postAuthor = postAuthor;
        }
    }

    public static class CommentBean implements Serializable{
        /**
         * id : 3137
         * createtime : 1525310838000
         * content : {"text":"999","images":[]}
         * parentId : null
         * childComments : [{"id":3168,"createtime":"1525831891000","content":"6666","parentId":3137,"childComments":null,"commentUserAvatar":null,"objectId":2849,"commentUserNickname":null,"replyUserId":2920,"replyUserAvatar":"https://panserver.solarsource.cn/panserver/files/a9536f12-30f1-4b52-8f25-ab900552e4f0/download","commentUserId":2982,"objectType":2,"replyUserNickname":"张顺运"},{"id":3169,"createtime":"1525831903000","content":"1234","parentId":3137,"childComments":null,"commentUserAvatar":null,"objectId":2849,"commentUserNickname":null,"replyUserId":2920,"replyUserAvatar":"https://panserver.solarsource.cn/panserver/files/a9536f12-30f1-4b52-8f25-ab900552e4f0/download","commentUserId":2982,"objectType":2,"replyUserNickname":"张顺运"}]
         * commentUserAvatar : https://panserver.solarsource.cn/panserver/files/a9536f12-30f1-4b52-8f25-ab900552e4f0/download
         * objectId : 2849
         * commentUserNickname : 张顺运
         * replyUserId : null
         * replyUserAvatar : null
         * commentUserId : 2920
         * objectType : 2
         * replyUserNickname : null
         */

        private int id;
        private String createtime;
        private String content;
        private Object parentId;
        private String commentUserAvatar;
        private int objectId;
        private String commentUserNickname;
        private Object replyUserId;
        private Object replyUserAvatar;
        private int commentUserId;
        private int objectType;
        private Object replyUserNickname;
        private List<ChildCommentsBean> childComments;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Object getParentId() {
            return parentId;
        }

        public void setParentId(Object parentId) {
            this.parentId = parentId;
        }

        public String getCommentUserAvatar() {
            return commentUserAvatar;
        }

        public void setCommentUserAvatar(String commentUserAvatar) {
            this.commentUserAvatar = commentUserAvatar;
        }

        public int getObjectId() {
            return objectId;
        }

        public void setObjectId(int objectId) {
            this.objectId = objectId;
        }

        public String getCommentUserNickname() {
            return commentUserNickname;
        }

        public void setCommentUserNickname(String commentUserNickname) {
            this.commentUserNickname = commentUserNickname;
        }

        public Object getReplyUserId() {
            return replyUserId;
        }

        public void setReplyUserId(Object replyUserId) {
            this.replyUserId = replyUserId;
        }

        public Object getReplyUserAvatar() {
            return replyUserAvatar;
        }

        public void setReplyUserAvatar(Object replyUserAvatar) {
            this.replyUserAvatar = replyUserAvatar;
        }

        public int getCommentUserId() {
            return commentUserId;
        }

        public void setCommentUserId(int commentUserId) {
            this.commentUserId = commentUserId;
        }

        public int getObjectType() {
            return objectType;
        }

        public void setObjectType(int objectType) {
            this.objectType = objectType;
        }

        public Object getReplyUserNickname() {
            return replyUserNickname;
        }

        public void setReplyUserNickname(Object replyUserNickname) {
            this.replyUserNickname = replyUserNickname;
        }

        public List<ChildCommentsBean> getChildComments() {
            return childComments;
        }

        public void setChildComments(List<ChildCommentsBean> childComments) {
            this.childComments = childComments;
        }

        public static class ChildCommentsBean implements Serializable{
            /**
             * id : 3168
             * createtime : 1525831891000
             * content : 6666
             * parentId : 3137
             * childComments : null
             * commentUserAvatar : null
             * objectId : 2849
             * commentUserNickname : null
             * replyUserId : 2920
             * replyUserAvatar : https://panserver.solarsource.cn/panserver/files/a9536f12-30f1-4b52-8f25-ab900552e4f0/download
             * commentUserId : 2982
             * objectType : 2
             * replyUserNickname : 张顺运
             */

            private int id;
            private String createtime;
            private String content;
            private int parentId;
            private Object childComments;
            private Object commentUserAvatar;
            private int objectId;
            private Object commentUserNickname;
            private int replyUserId;
            private String replyUserAvatar;
            private int commentUserId;
            private int objectType;
            private String replyUserNickname;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public Object getChildComments() {
                return childComments;
            }

            public void setChildComments(Object childComments) {
                this.childComments = childComments;
            }

            public Object getCommentUserAvatar() {
                return commentUserAvatar;
            }

            public void setCommentUserAvatar(Object commentUserAvatar) {
                this.commentUserAvatar = commentUserAvatar;
            }

            public int getObjectId() {
                return objectId;
            }

            public void setObjectId(int objectId) {
                this.objectId = objectId;
            }

            public Object getCommentUserNickname() {
                return commentUserNickname;
            }

            public void setCommentUserNickname(Object commentUserNickname) {
                this.commentUserNickname = commentUserNickname;
            }

            public int getReplyUserId() {
                return replyUserId;
            }

            public void setReplyUserId(int replyUserId) {
                this.replyUserId = replyUserId;
            }

            public String getReplyUserAvatar() {
                return replyUserAvatar;
            }

            public void setReplyUserAvatar(String replyUserAvatar) {
                this.replyUserAvatar = replyUserAvatar;
            }

            public int getCommentUserId() {
                return commentUserId;
            }

            public void setCommentUserId(int commentUserId) {
                this.commentUserId = commentUserId;
            }

            public int getObjectType() {
                return objectType;
            }

            public void setObjectType(int objectType) {
                this.objectType = objectType;
            }

            public String getReplyUserNickname() {
                return replyUserNickname;
            }

            public void setReplyUserNickname(String replyUserNickname) {
                this.replyUserNickname = replyUserNickname;
            }
        }
    }

    public static class LikesBean implements Serializable{
        /**
         * id : 2187
         * createtime : 1525311894000
         * avatar : null
         * objectType : 2
         * nickName : null
         * objectId : 2854
         * userId : 2369
         */

        private int id;
        private String createtime;
        private Object avatar;
        private int objectType;
        private Object nickName;
        private int objectId;
        private int userId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public int getObjectType() {
            return objectType;
        }

        public void setObjectType(int objectType) {
            this.objectType = objectType;
        }

        public Object getNickName() {
            return nickName;
        }

        public void setNickName(Object nickName) {
            this.nickName = nickName;
        }

        public int getObjectId() {
            return objectId;
        }

        public void setObjectId(int objectId) {
            this.objectId = objectId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        @Override
        public String toString() {
            return "LikesBean{" +
                    "id=" + id +
                    ", createtime='" + createtime + '\'' +
                    ", avatar=" + avatar +
                    ", objectType=" + objectType +
                    ", nickName=" + nickName +
                    ", objectId=" + objectId +
                    ", userId=" + userId +
                    '}';
        }
    }
}
