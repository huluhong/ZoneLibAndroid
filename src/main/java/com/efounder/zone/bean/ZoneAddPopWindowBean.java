package com.efounder.zone.bean;


/**
 * 点击+弹出popwindow的bean
 */
public class ZoneAddPopWindowBean {
    private int drawable;
    private String name;
    private boolean isChecked;
    //是否允许被选中
    private boolean canChecked;


    public ZoneAddPopWindowBean(String name, int drawable,  boolean isChecked, boolean canChecked) {
        this.drawable = drawable;
        this.name = name;
        this.isChecked = isChecked;
        this.canChecked = canChecked;
    }

    public int getDrawable() {
        return drawable;
    }

    public ZoneAddPopWindowBean setDrawable(int drawable) {
        this.drawable = drawable;
        return this;
    }

    public String getName() {
        return name;
    }

    public ZoneAddPopWindowBean setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public ZoneAddPopWindowBean setChecked(boolean checked) {
        isChecked = checked;
        return this;
    }

    public boolean isCanChecked() {
        return canChecked;
    }

    public ZoneAddPopWindowBean setCanChecked(boolean canChecked) {
        this.canChecked = canChecked;
        return this;
    }
}
