package com.efounder.zone.bean;

public class ZoneMessageBean {

    // 1:点赞， 2:评论， 3.评论回复, 4. 转发
    private int aboutType;

    private String creatime;

    private int userid;

    private int objType;

    //如果为空，则原说说已删除
    private TalksBean.ObjBean objBean;

    private TalksBean.CommentBean commentBean;

    private ZoneMessageBean(Builder builder) {
        aboutType = builder.aboutType;
        creatime = builder.creatime;
        userid = builder.userid;
        objType = builder.objType;
        objBean = builder.objBean;
        commentBean = builder.commentBean;
    }

    public int getAboutType() {
        return aboutType;
    }

    public String getCreatime() {
        return creatime;
    }

    public int getUserid() {
        return userid;
    }

    public int getObjType() {
        return objType;
    }

    public TalksBean.ObjBean getObjBean() {
        return objBean;
    }

    public TalksBean.CommentBean getCommentBean() {
        return commentBean;
    }

    public static final class Builder {
        private int aboutType;
        private String creatime;
        private int userid;
        private int objType;
        private TalksBean.ObjBean objBean;
        private TalksBean.CommentBean commentBean;

        public Builder() {
        }

        public Builder aboutType(int val) {
            aboutType = val;
            return this;
        }

        public Builder creatime(String val) {
            creatime = val;
            return this;
        }

        public Builder userid(int val) {
            userid = val;
            return this;
        }

        public Builder objType(int val) {
            objType = val;
            return this;
        }

        public Builder objBean(TalksBean.ObjBean val) {
            objBean = val;
            return this;
        }

        public Builder commentBean(TalksBean.CommentBean val) {
            commentBean = val;
            return this;
        }

        public ZoneMessageBean build() {
            return new ZoneMessageBean(this);
        }
    }
}
