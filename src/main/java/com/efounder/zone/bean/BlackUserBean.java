package com.efounder.zone.bean;

/**
 * 黑名单人员bean
 */
public class BlackUserBean {

    private String useId;
    private String userName;
    private boolean isBlack;
    private Object avatar;

    private BlackUserBean(Builder builder) {
        setUseId(builder.useId);
        setUserName(builder.userName);
        setBlack(builder.isBlack);
        setAvatar(builder.avatar);
    }

    public String getUseId() {
        return useId;
    }

    public BlackUserBean setUseId(String useId) {
        this.useId = useId;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public BlackUserBean setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public boolean isBlack() {
        return isBlack;
    }

    public BlackUserBean setBlack(boolean black) {
        isBlack = black;
        return this;
    }

    public Object getAvatar() {
        return avatar;
    }

    public BlackUserBean setAvatar(Object avatar) {
        this.avatar = avatar;
        return this;
    }


    public static final class Builder {
        private String useId;
        private String userName;
        private boolean isBlack;
        private Object avatar;

        public Builder() {
        }

        public Builder useId(String val) {
            useId = val;
            return this;
        }

        public Builder userName(String val) {
            userName = val;
            return this;
        }

        public Builder isBlack(boolean val) {
            isBlack = val;
            return this;
        }

        public Builder avatar(Object val) {
            avatar = val;
            return this;
        }

        public BlackUserBean build() {
            return new BlackUserBean(this);
        }
    }
}
