package com.efounder.zone.activity;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.efounder.chat.activity.BaseActivity;
import com.efounder.chat.utils.ImageUtil;
import com.efounder.chat.utils.PansoftCloudUtil;
import com.efounder.pansoft.chat.photo.JFMessagePicturePickView;
import com.efounder.pansoft.chat.photo.JFPicturePickPhotoWallActivity;
import com.efounder.ui.util.DisplayUtil;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.ToastUtil;
import com.efounder.zone.R;
import com.efounder.zone.adapter.WriteSelectImageAdapter;
import com.efounder.zone.bean.TalksBean;
import com.efounder.zone.event.MobileZoneSendContentEvent;
import com.efounder.zone.util.CommentUtil;
import com.efounder.zone.util.CreateBeanUtil;
import com.utilcode.util.DeviceUtils;
import com.utilcode.util.JFStringUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.efounder.chat.fragment.ChatSenderFragment.chatpath;
import static com.efounder.pansoft.chat.photo.JFMessagePicturePickView.REQUEST_PIC_SELECTE_CODE;

/**
 * 写内容界面
 *
 * @author yqs
 */
public class WriteContentActivity extends BaseActivity {
//
//    private static final String TAG = "WriteContentActivity";
//    private ImageView ivBack;
//    private TextView tvTitle;
//    private ImageView ivAdd;
//    private TextView tvSend;
//
//    private EditText etInput;
//    private LinearLayout llPhoto;
//    private RelativeLayout llWebLayout;
//    private EditText etUrl;
//    private SeekBar seekbar;
//    private WebView webView;
//    private GridView gridView;
//    private TextView tvWebViewHeightView;
//
//    private NeiKongSelectImageAdapter adapter;
//
//    private String mTitle;
//    //动态类型 说说为1 网页为2
//    private int mDynamicType = 0;
//    private String deviceModel;
//
//    private ArrayList<String> mPictureList = new ArrayList<>();//和选图联动的图片list
//    private List<String> imgAddressList = new ArrayList<>();//给adapter传入的图片list
//    private ArrayList<String> imgUrlList = new ArrayList<>();//图片的url list
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.mobile_zone_activity_write_content);
//
//        mTitle = getIntent().getStringExtra("title");
//        if ("说说".equals(mTitle)) {
//            mDynamicType = 1;
//        } else {
//            mDynamicType = 2;
//        }
//        deviceModel = BaseDeviceUtils.getModel();
//        initView();
//        initData();
//        initListener();
//
//    }
//
//    private void initView() {
//        ivBack = (ImageView) findViewById(R.id.iv_back);
//        tvTitle = (TextView) findViewById(R.id.tv_title);
//        ivAdd = (ImageView) findViewById(R.id.iv_add);
//        tvSend = (TextView) findViewById(R.id.tv_save);
//        tvSend.setVisibility(View.VISIBLE);
//        etInput = (EditText) findViewById(R.id.et_input);
//        llPhoto = (LinearLayout) findViewById(R.id.ll_photo);
//        llWebLayout = (RelativeLayout) findViewById(R.id.ll_web_layout);
//        etUrl = (EditText) findViewById(R.id.et_url);
//        seekbar = (SeekBar) findViewById(R.id.seekbar);
//        webView = (WebView) findViewById(R.id.webView);
//        gridView = (GridView) findViewById(R.id.gridview);
//        tvWebViewHeightView = (TextView) findViewById(R.id.tv_title2);
//
//    }
//
//    private void initListener() {
//
//        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                //加号，选择图片
//                if (position == imgAddressList.size()) {
//                    Intent intent = new Intent(WriteContentActivity.this, JFPicturePickPhotoWallActivity.class);
//                    intent.putExtra("mPictureList", mPictureList);
//                    intent.putExtra(JFPicturePickPhotoWallActivity.MAX_NUM, 9);//可选择的最大照片数
//                    intent.putExtra(JFPicturePickPhotoWallActivity.SHOW_RAW_PIC, false);//不显示原图
//                    intent.putExtra("sendBtnName", "确定");
//                    startActivityForResult(intent, REQUEST_PIC_SELECTE_CODE);
//                    overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
//                }
//            }
//        });
//        adapter.setOnImgRemovedListener(new NeiKongSelectImageAdapter.OnImgRemovedListener() {
//            @Override
//            public void onImgRemoved(int position) {
//                imgAddressList.remove(position);
//                mPictureList.remove(position);
//                adapter.notifyDataSetChanged();
//            }
//        });
//
//        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                LinearLayout.LayoutParams layoutParams =
//                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                                DisplayUtil.dip2px(WriteContentActivity.this, 200 + progress));
//                webView.setLayoutParams(layoutParams);
//                tvWebViewHeightView.setText(String.format("调整网页高度%1$d", 200 + progress));
//
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//
//            }
//        });
//        tvSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendNewDynamic();
//            }
//        });
//    }
//
//    /**
//     * 发送新动态
//     */
//    private void sendNewDynamic() {
//        String content = etInput.getText().toString();
//
//        String urlStr = etUrl.getText().toString();
//        // 类型为说说
//        if (mDynamicType == 1) {
////            文字 图片都为空
//            if ("".equals(content) && imgAddressList.size() == 0) {
//                ToastUtil.showToast(this, "说说内容为空");
//            }
//            // 图片不为空
//            else if (imgAddressList.size() > 0) {
//                LoadingDataUtilBlack.show(this, "请稍候...");
//                //压缩图片上传
//                if(imgAddressList.size() == 1){
//                    String imgLocation = imgAddressList.get(0);
//                    //是gif图 不压缩
//                    if(ImageUtil.isGifFile((new File(imgLocation)))){
//                        uploadOneImage(imgLocation, ImageUtil.getPicScale(imgLocation));
//                    }else{
//                        String scale = ImageUtil.saveNewImage(imgLocation, 1280, 1280);
//                        String img = chatpath + ImageUtil.getFileName(imgLocation) + ".pic";
//                        uploadOneImage(img, scale);
//                    }
//                }else{
//                    for (String imgAddress : imgAddressList) {
//                        //如果不是gif动图则进行裁剪压缩
//                        if (!ImageUtil.isGifFile((new File(imgAddress)))) {
//                            ImageUtil.saveNewImage(imgAddress, 1280, 1280);
//                            uploadImage(chatpath + ImageUtil.getFileName(imgAddress) + ".pic");
//                        } else {
//                            uploadImage(imgAddress);
//                        }
//                    }
//                }
//            } else {
//                //图片为空
//                sendOnlyText();
//            }
//        } else {
//            if ("".equals(urlStr)) {
//                ToastUtil.showToast(this, "网址为空");
//
//            } else {
//                sendUrl();
//            }
//        }
//
//    }
//
//    private void initData() {
//        tvSend.setText("发表");
//
//        tvTitle.setText(mTitle);
//        if (mDynamicType == 1) {
//            llWebLayout.setVisibility(View.GONE);
//        } else {
//            llPhoto.setVisibility(View.GONE);
//        }
//        adapter = new NeiKongSelectImageAdapter(this, imgAddressList);
//        gridView.setAdapter(adapter);
//        seekbar.setMax(200);
//        seekbar.setProgress(0);
//        if (mDynamicType == 2) {
//            //获取剪贴板管理器：
//            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
//            if (cm.getText() != null) {
//                String text = cm.getText().toString();
//                if (JFStringUtil.isHttpUrl(text)) {
//                    LinearLayout.LayoutParams layoutParams =
//                            new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                                    DisplayUtil.dip2px(WriteContentActivity.this, 200));
//                    webView.setLayoutParams(layoutParams);
//                    if ((!text.startsWith("http://")) && (!text.startsWith("https://"))) {
//                        text = "https://" + text;
//                    }
//                    //如果不设置WebViewClient，请求会跳转系统浏览器
//                    webView.setWebViewClient(new WebViewClient() {
//                        //覆盖shouldOverrideUrlLoading 方法
//                        @Override
//                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                            view.loadUrl(url);
//                            return true;
//                        }
//                    });
//                    webView.loadUrl(text);
//                    etUrl.setText(text);
//                }
//            }
//        }
//
//
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == JFMessagePicturePickView.REQUEST_PIC_SELECTE_CODE
//                && resultCode == RESULT_OK) {
//            //图库选择照片
//            imgAddressList.clear();
//            imgAddressList.addAll((ArrayList<String>) data.getExtras().get("mSelectedPics"));
//            mPictureList.clear();
//            mPictureList.addAll((ArrayList<String>) data.getExtras().get("mSelectedPics"));
//            adapter.notifyDataSetChanged();
//        }
//    }
//
//    /**
//     * 向云盘上传图片，上传完成后发送图片
//     *
//     * @param imgAddress 图片的本地路径
//     */
//    private void uploadImage(String imgAddress) {
//        PansoftCloudUtil.getCloudRes(imgAddress, new PansoftCloudUtil.UpLoadListener() {
//            @Override
//            public void getHttpUrl(Boolean isSuccess, String url) {
//                if (isSuccess) {
//                    Log.i(TAG, "---头像在云盘服务器上的路径:" + url);
//                    imgUrlList.add(url);
//                    if (imgAddressList.size() == imgUrlList.size()) {
//                        LoadingDataUtilBlack.dismiss();
//                        sendImg();
//                    }
//                } else {
//                    LoadingDataUtilBlack.dismiss();
//                    imgUrlList.clear();
//                    ToastUtil.showToast(WriteContentActivity.this, "发送出错请重试");
//                }
//            }
//        });
//    }
//
//    /**
//     * 上传单张图片并发送
//     * @param imgLocation
//     * @param scale
//     */
//    private void uploadOneImage(String imgLocation, String scale){
//        String[]  scales = scale.split(":");
//        final String strWidth = scales[0];
//        final String strHeight = scales[1];
//
//        final List<String> imglist = new ArrayList<>();
//
//        PansoftCloudUtil.getCloudRes(imgLocation, new PansoftCloudUtil.UpLoadListener() {
//            @Override
//            public void getHttpUrl(Boolean isSuccess, String url) {
//                if (isSuccess) {
//                    Log.i(TAG, "---头像在云盘服务器上的路径:" + url);
//                    imglist.add(url);
//                    LoadingDataUtilBlack.dismiss();
//                    String contentText = etInput.getText().toString();
//                    String content = new CreateBeanUtil().createSingleImageContent(contentText, imglist,Integer.valueOf(strWidth), Integer.valueOf(strHeight));
//                    TalksBean talksBean = new CreateBeanUtil().createTalksBean(2, deviceModel, content);
//
//                    EventBus.getDefault().post(new MobileZoneSendContentEvent(talksBean));
//
//                    finish();
//                } else {
//                    LoadingDataUtilBlack.dismiss();
//                    ToastUtil.showToast(WriteContentActivity.this, "发送出错请重试");
//                }
//            }
//        });
//    }
//
//    /**
//     * 只发送文字
//     */
//    private void sendOnlyText() {
//        String contentText = etInput.getText().toString();
////        String content = CreateTalksBeanUtil.createCommonContent(contentText, new ArrayList<String>());
//        String content = new CreateBeanUtil().createCommonContent(contentText, new ArrayList<String>());
//
////        TalksBean talksBean = CreateTalksBeanUtil.createTalksBean(2, deviceModel, content);
//        TalksBean talksBean = new CreateBeanUtil().createTalksBean(2, deviceModel, content);
//        EventBus.getDefault().post(new MobileZoneSendContentEvent(talksBean));
//
//        finish();
//    }
//
//    /**
//     * 发送网址
//     */
//    private void sendUrl() {
//        String contentText = etInput.getText().toString();
//        String urlText = etUrl.getText().toString();
//        int webHeight = 200 + seekbar.getProgress();
//
////        String content = CreateTalksBeanUtil.createWebContent(contentText, urlText, webHeight);
////        TalksBean talksBean = CreateTalksBeanUtil.createTalksBean(2, deviceModel, content);
//        String content = new CreateBeanUtil().createWebContent(contentText, urlText, webHeight);
//        TalksBean talksBean = new CreateBeanUtil().createTalksBean(2, deviceModel, content);
//
//
//        EventBus.getDefault().post(new MobileZoneSendContentEvent(talksBean));
//
//        finish();
//
//    }
//
//    /**
//     * 发送图片
//     */
//    private void sendImg() {
//        String contentText = etInput.getText().toString();
////        String content = CreateTalksBeanUtil.createCommonContent(contentText, imgUrlList);
////        TalksBean talksBean = CreateTalksBeanUtil.createTalksBean(2, deviceModel, content);
//        String content = new CreateBeanUtil().createCommonContent(contentText, imgUrlList);
//        TalksBean talksBean = new CreateBeanUtil().createTalksBean(2, deviceModel, content);
//
//
//        EventBus.getDefault().post(new MobileZoneSendContentEvent(talksBean));
//
//        finish();
//
//    }
//
}
