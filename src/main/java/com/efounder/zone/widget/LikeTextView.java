package com.efounder.zone.widget;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import com.efounder.chat.db.WeChatDBManager;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.utils.JfResourceUtil;
import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;
import com.efounder.zone.bean.TalksBean;

import java.util.List;

/**
 * 点赞好友列表组件
 *
 * @author YQS 2018/05/09
 */
public class LikeTextView extends AppCompatTextView {

    private Context mContext;
    //点赞数据
    private List<TalksBean.LikesBean> likesBeanList;

    public LikeTextView(Context context) {
        this(context, null);
    }

    public LikeTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LikeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        this.setTextColor(JfResourceUtil.getSkinColor(R.color.black_text_color));
        this.setGravity(Gravity.CENTER_VERTICAL);

    }

    /**
     * 新增点赞
     *
     * @param bean
     */
    public void addLikeBean(TalksBean.LikesBean bean) {
        if (likesBeanList != null) {
            likesBeanList.add(bean);
            notifyDataSetChanged();
        }
    }

    /**
     * 设置点赞数据
     *
     * @param list
     */
    public void setLikeBeanList(List<TalksBean.LikesBean> list) {
        this.likesBeanList = list;
        this.setText("");
        if (likesBeanList != null) {
            notifyDataSetChanged();
        }
    }

    /**
     * 刷新点赞列表
     */
    public void notifyDataSetChanged() {
        if (likesBeanList == null || likesBeanList.size() <= 0) {
            setText("");
            return;
        }
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(setImageSpan());
        for (int i = 0; i < likesBeanList.size(); i++) {

            TalksBean.LikesBean item = likesBeanList.get(i);

            //查询取出用户名

            String userName = null;
            if (item.getNickName()!=null && !"".equals(item.getNickName())){
                userName = (String) item.getNickName();
            }else{
                final User user = WeChatDBManager.getInstance().getOneUserById(item.getUserId());
                 userName = user.getNickName();
            }
            if (i != likesBeanList.size() - 1) {
                userName = userName + "、";
            }

            builder.append(setClickableSpan(userName, item));

        }
        builder.append(ResStringUtil.getString(R.string.zone_wait)).append(likesBeanList.size() + "").append(ResStringUtil.getString(R.string.zone_people_think_zan));
        setText(builder);
        setMovementMethod(new CircleMovementMethod(0x00000000, 0x00000000));
    }

    /**
     * 设置评论用户名字点击事件
     *
     * @param userName 点赞人名称
     * @param bean     点赞json
     * @return
     */
    public SpannableString setClickableSpan(final String userName, final TalksBean.LikesBean bean) {

        final SpannableString string = new SpannableString(userName);
        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                // TODO:  评论用户名字点击事件
                //Toast.makeText(mContext, userName, Toast.LENGTH_SHORT).show();
                if (listener != null) {
                    listener.onItemClick(bean);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                // 设置显示的文字颜色
                ds.setColor(JfResourceUtil.getSkinColor(R.color.mobile_zone_like_comment_people_textcolor));
                ds.setUnderlineText(false);
            }
        };

        string.setSpan(span, 0, string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return string;
    }

    /**
     * 设置点赞图标
     *
     * @return
     */
    private SpannableString setImageSpan() {
        String text = "  ";
        SpannableString imgSpanText = new SpannableString(text);
        imgSpanText.setSpan(new ImageSpan(getContext(), R.drawable.mobile_zone_like_icon1, DynamicDrawableSpan.ALIGN_BOTTOM),
                0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return imgSpanText;
    }


    private onItemClickListener listener;

    public void setListener(onItemClickListener listener) {
        this.listener = listener;
    }

    public interface onItemClickListener {
        void onItemClick(TalksBean.LikesBean bean);
    }
}
