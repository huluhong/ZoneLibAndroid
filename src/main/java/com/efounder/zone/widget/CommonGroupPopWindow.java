package com.efounder.zone.widget;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.efounder.zone.R;


/**
 * 分组popupwindow
 *
 * @author YQS
 */
@Deprecated
public class CommonGroupPopWindow extends PopupWindow {
    private LinearLayout conentView;
    LayoutInflater inflater;
    private Context context;
    private PopWindowItemOnClickListener listener;

    public CommonGroupPopWindow(final Activity context) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = (LinearLayout) inflater.inflate(R.layout.popupwindow_common_group, null);
        this.context = context;

        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);

        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimationPreview);

    }

    public void addMenuButton(String[] menuName) {
//        ImageView iv_menuicon = menuView.findViewById(R.id.iv_menuicon);
//        iv_menuicon.setImageDrawable(context.getResources().getDrawable(menuIcon));

        for (int i = 0; i < menuName.length; i++) {

            View menuView = inflater.inflate(R.layout.popupwindow_common_group_item, null);
            if (i == 0) {
                ImageView arrow = (ImageView) menuView.findViewById(R.id.mobile_zone_top_arrow);
                arrow.setVisibility(View.VISIBLE);
            }
            final CheckBox checkBox = menuView.findViewById(R.id.cb_menuCheckBox);
            TextView menuNameView = menuView.findViewById(R.id.tv_menuname);
            menuNameView.setText(menuName[i]);
            conentView.addView(menuView);
            final int finalI = i;
            menuView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view, finalI);
                    checkBox.setChecked(!checkBox.isChecked());
                    dismiss();
                }
            });
        }

//        return menuView;

    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, 0, 0);
        } else {
            this.dismiss();
        }
    }

    public interface PopWindowItemOnClickListener {
        public void onClick(View view, int position);
    }

    public void setItemOnClickListener(PopWindowItemOnClickListener listener) {
        this.listener = listener;
    }
}
