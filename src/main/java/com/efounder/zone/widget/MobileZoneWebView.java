package com.efounder.zone.widget;

import android.content.Context;
import android.net.http.SslError;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.efounder.chat.utils.NetUtils;
import com.efounder.view.RoundFrameLayout;
import com.efounder.zone.R;
import com.utilcode.util.ScreenUtils;
import com.utilcode.util.SizeUtils;

/**
 * Created by Richard on 2018/5/15.
 * <p>
 * 拦截点击事件的webview
 */

public class MobileZoneWebView extends RoundFrameLayout {

    private View webViewClickButton;
    private WebView webView;

    public MobileZoneWebView(Context context) {
        super(context);
    }

    public MobileZoneWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MobileZoneWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    private void initView() {
        View.inflate(getContext(), R.layout.mobile_zone_webview, this);
        webView = findViewById(R.id.mobile_zone_webview_frame);
        webViewClickButton = findViewById(R.id.mobile_zone_webview_click);
//                mWebView.clearCache(true);
        webView.setWebViewClient(new WebViewClient() {//对https支持
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();


            }
        });

        //设置layout宽度 为 屏幕宽度 -20dp  设置webview宽高
        int layoutWidth = ScreenUtils.getScreenWidth() - SizeUtils.dp2px(10) * 2;
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(layoutWidth, layoutWidth * 2 / 3);
        webView.setLayoutParams(params);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //View级别
            //您可以在运行时用以下的代码关闭单个view的硬件加速：
            //20200114 关闭硬件加速，解决webview 在recyclerview 偏移的问题
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        //允许JavaScript执行
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheMaxSize(10 * 1024 * 1024);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (NetUtils.hasNetwork(getContext())) {
            settings.setCacheMode(WebSettings.LOAD_DEFAULT);  //设置 缓存模式
        } else {
            settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);  //设置 缓存模式
        }
// 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        //开启 database storage API 功能
        settings.setDatabaseEnabled(true);
        //开启 Application Caches 功能
        settings.setAppCacheEnabled(true);

    }

    public View getWebClickView() {
        return webViewClickButton;
    }

    public void loadUrl(String url) {
        if (!url.startsWith("http")) {
            url = "http://" + url;
        }
        webView.loadUrl(url);
    }
}
