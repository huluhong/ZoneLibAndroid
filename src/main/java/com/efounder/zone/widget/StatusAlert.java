package com.efounder.zone.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.efounder.zone.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * @author will
 * 成功或失败后从顶部的弹窗
 */
public class StatusAlert {

    /**
     * 成功弹窗
     * @param context context
     * @param rootView 显示页面的主view
     * @param alert 显示的文字
     */
    public static void successAlert(Context context, View rootView, String alert) {
        if (((Activity)context).isFinishing() ||((Activity)context).isDestroyed()){
            return;
        }
        View contentView;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(R.layout.mobile_zone_success_alert, null);
        final PopupWindow popup = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        TextView tvAlert = (TextView) contentView.findViewById(R.id.tv_alert);
        if (!alert.equals(""))
        tvAlert.setText(alert);
        popup.setContentView(contentView);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.setOutsideTouchable(false);
        popup.setAnimationStyle(R.style.zone_pop_anim);
        popup.showAtLocation(rootView, Gravity.TOP, 0, 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                popup.dismiss();
            }
        }, 3000);
    }

    /**
     * 失败弹窗
     * @param context context
     * @param rootView 显示页面的主view
     * @param alert 显示的文字
     */
    public static void failAlert(Context context, View rootView, String alert) {
        View contentView;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(R.layout.mobile_zone_fail_alert, null);
        final PopupWindow popup = new PopupWindow(contentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        TextView tvAlert = (TextView) contentView.findViewById(R.id.tv_alert);
        if (!alert.equals(""))
            tvAlert.setText(alert);
        popup.setContentView(contentView);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.setOutsideTouchable(false);
        popup.setAnimationStyle(R.style.zone_pop_anim);
        popup.showAtLocation(rootView, Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                popup.dismiss();
            }
        }, 3000);
    }
}
