package com.efounder.zone.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.efounder.baseadapter.listview.CommonAdapter;
import com.efounder.baseadapter.listview.ViewHolder;
import com.efounder.chat.activity.RecorderActivity;
import com.efounder.utils.JfResourceUtil;
import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;
import com.efounder.zone.activity.WriteContentActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 空间加号按钮PopWindow
 *
 * @author YQS 2018/05/09
 */
public class AddZonePopWindow extends PopupWindow {

    private LinearLayout conentView;
    LayoutInflater inflater;
    private Context mContext;
    private ImageView ivClose;
    private GridView gridview;
    private GridViewAdapter adapter;

    private List<AddMenu> addMenuList;
    private Fragment fragment;

    //拍摄视频
    public static final int REQUEST_CODE_SELECT_VIDEO = 200;

    public AddZonePopWindow(final Fragment fragment) {
        this(fragment.getActivity());
        this.fragment = fragment;
    }

    public AddZonePopWindow(final Context context) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = (LinearLayout) inflater.inflate(R.layout.mobile_zone_add_popwindow, null);
        this.mContext = context;

        ivClose = (ImageView) conentView.findViewById(R.id.iv_close);
        gridview = (GridView) conentView.findViewById(R.id.gridview);


        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);
        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimationPreview);

        initData();

    }

    private void initData() {
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        addMenuList = new ArrayList<>();
        addMenuList.add(new AddMenu(ResStringUtil.getString(R.string.zone_ss), R.drawable.mobile_zone_shuoshuo_icon));
        addMenuList.add(new AddMenu(ResStringUtil.getString(R.string.zone_video), R.drawable.mobile_zone_add_video_icon));
        addMenuList.add(new AddMenu(ResStringUtil.getString(R.string.zone_web), R.drawable.mobile_zong_add_web_icon));

        adapter = new GridViewAdapter(mContext, R.layout.mobile_zone_item_add_popwindow_grid_item, addMenuList);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(mContext, WriteContentActivity.class);
                    intent.putExtra("title", addMenuList.get(position).name);
                    mContext.startActivity(intent);
                } else if (position == 1) {

                    if (fragment != null) {
                        Intent intent = new Intent(mContext, RecorderActivity.class);
                        fragment.startActivityForResult(intent, AddZonePopWindow.REQUEST_CODE_SELECT_VIDEO);
                        ((Activity) mContext).overridePendingTransition(R.anim.push_bottom_in, R.anim.push_top_out);
                    } else {
                        Intent intent = new Intent(mContext, RecorderActivity.class);
                        ((Activity) mContext).startActivityForResult(intent, AddZonePopWindow.REQUEST_CODE_SELECT_VIDEO);
                        ((Activity) mContext).overridePendingTransition(R.anim.push_bottom_in, R.anim.push_top_out);
                    }
                } else if (position == 2) {
                    Intent intent = new Intent(mContext, WriteContentActivity.class);
                    intent.putExtra("title", addMenuList.get(position).name);
                    mContext.startActivity(intent);
                }
                dismiss();
            }

        });

    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        WindowManager.LayoutParams params = ((Activity) mContext).getWindow().getAttributes();
        params.alpha = 0.7f;

        ((Activity) mContext).getWindow().setAttributes(params);
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            //this.showAsDropDown(parent,  0, 0);
            showAtLocation(parent,
                    Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        } else {
            this.dismiss();
        }
    }


    public static class AddMenu {
        public String name;
        public int icon;


        public AddMenu(String name, int icon) {
            this.name = name;
            this.icon = icon;
        }
    }


    private static class GridViewAdapter extends CommonAdapter {
        private List<AddMenu> datas;
        private Context mContext;

        public GridViewAdapter(Context context, int layoutId, List<AddMenu> datas) {
            super(context, layoutId, datas);
            this.datas = datas;
            this.mContext = context;
        }


        @Override
        protected void convert(ViewHolder viewHolder, Object item, int position) {
            TextView titleView = viewHolder.getView(R.id.tv_title12);
            ImageView imageView = viewHolder.getView(R.id.iv_icon);

            titleView.setText(datas.get(position).name);
            titleView.setTextColor(JfResourceUtil.getSkinColor(R.color.black_text_color));
            imageView.setImageDrawable(mContext.getResources().getDrawable(datas.get(position).icon));
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        WindowManager.LayoutParams params = ((Activity) mContext).getWindow().getAttributes();
        params.alpha = 1f;

        ((Activity) mContext).getWindow().setAttributes(params);
    }


}
