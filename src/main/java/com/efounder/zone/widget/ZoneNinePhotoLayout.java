package com.efounder.zone.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.efounder.chat.activity.ViewPagerActivity;
import com.efounder.utils.JfResourceUtil;
import com.efounder.zone.R;
import com.utilcode.util.ScreenUtils;
import com.utilcode.util.SizeUtils;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * 空间展示图片组件
 */
public class ZoneNinePhotoLayout extends RelativeLayout {
    private static final String TAG = "ZoneNinePhotoLayout";

    private Context mContext;

    //屏幕宽度
    private int screenWidth;

    //图片url
    private List<String> urlList;
    //imageview 数组
    private List<ImageView> imageViewList;
    //图片item之间的距离
    private int itemWhiteSpace;

    //当前RelativeLayout 的宽度
    private int layoutWidth;
    //当前RelativeLayout 的高度
    private int layoutHeight;

    //图片全部圆角
    MultiTransformation multiAll;
    //图片左上圆角
    MultiTransformation multiLeftTop;
    MultiTransformation multiTopRight;
    MultiTransformation multiBottomRight;
    MultiTransformation multibottomLeft;
    MultiTransformation multiLeft;
    MultiTransformation multiRight;

    //imageview 图片点击事件
    private onPhotoItmeClickListener itemClickListener;

    public ZoneNinePhotoLayout(Context context) {
        this(context, null);
    }

    public ZoneNinePhotoLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ZoneNinePhotoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initParam();
    }

    private void initParam() {
        screenWidth = ScreenUtils.getScreenWidth();

        //设置图片item之间距离 4dp
        itemWhiteSpace = SizeUtils.dp2px(4);
        imageViewList = new ArrayList<>();
        //设置layout宽度 为 屏幕宽度 -20dp
        layoutWidth = screenWidth - SizeUtils.dp2px(10) * 2;
        LayoutParams params = new LayoutParams(layoutWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(params);

        multiAll = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0,
                        RoundedCornersTransformation.CornerType.ALL));
        multiLeftTop = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0,
                        RoundedCornersTransformation.CornerType.TOP_LEFT));
        multiTopRight = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0,
                        RoundedCornersTransformation.CornerType.TOP_RIGHT));
        multiBottomRight = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0,
                        RoundedCornersTransformation.CornerType.BOTTOM_RIGHT));
        multibottomLeft = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0,
                        RoundedCornersTransformation.CornerType.BOTTOM_LEFT));

        multiLeft = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0,
                        RoundedCornersTransformation.CornerType.LEFT));
        multiRight = new MultiTransformation(
                new CenterCrop(),
                new RoundedCornersTransformation(20, 0,
                        RoundedCornersTransformation.CornerType.RIGHT));

    }


    //设置数据
    public void setData(List<String> urlList) {
        imageViewList.clear();
        this.urlList = urlList;
        if (urlList == null) {
            return;
        }

        adjustLayout();
        createPhotoLayout();
        displayPhoto();


    }


    /**
     * 调整布局
     */
    private void adjustLayout() {
        this.removeAllViews();
        if (urlList.size() == 2) {
            layoutHeight = (layoutWidth - itemWhiteSpace) / 2;
        } else {
            layoutHeight = (layoutWidth - itemWhiteSpace) * 2 / 3;
        }
        Log.i(TAG, "adjustLayout: layoutWidth=" + layoutWidth);
        Log.i(TAG, "adjustLayout: layoutHeight=" + layoutHeight);
        getLayoutParams().height = layoutHeight;
    }

    /**
     * 创建图片布局
     */
    private void createPhotoLayout() {

        for (int i = 0; i < urlList.size(); i++) {
            RelativeLayout.LayoutParams params = createLayoutParam(i);
            final ImageView imageView = new ImageView(mContext);
            final int position = i;
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewPagerActivity.showImageWall(mContext, urlList.toArray(new String[0]), position, imageView);
                }
            });
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            setImageViewId(imageView, i);
            imageViewList.add(imageView);
            this.addView(imageView, params);
        }

    }

    /**
     * 设置imageivew的id
     *
     * @param view
     * @param position
     */
    private void setImageViewId(ImageView view, int position) {

        switch (position) {
            case 0:
                view.setId(R.id.zone_imageview0);
                break;
            case 1:
                view.setId(R.id.zone_imageview1);
                break;
            case 2:
                view.setId(R.id.zone_imageview2);
                break;
            case 3:
                view.setId(R.id.zone_imageview3);
                break;
            case 4:
                view.setId(R.id.zone_imageview4);
                break;
            case 5:
                view.setId(R.id.zone_imageview5);
                break;
            case 6:
                view.setId(R.id.zone_imageview6);
                break;
            case 7:
                view.setId(R.id.zone_imageview7);
                break;
        }
    }

    /**
     * 创建iamgeview的LayoutParams
     *
     * @param position 图片序号 第几张
     * @return
     */
    private RelativeLayout.LayoutParams createLayoutParam(int position) {
        //图片数量
        int iamgeSize = urlList.size();
        RelativeLayout.LayoutParams layoutParams1 = null;
        if (iamgeSize == 1) {
            layoutParams1 = new RelativeLayout.LayoutParams(
                    layoutWidth, layoutHeight);
            layoutParams1.addRule(RelativeLayout.CENTER_IN_PARENT);

        } else if (iamgeSize == 2) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        layoutHeight);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        layoutHeight);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            }

        } else if (iamgeSize == 3) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 3 * 2,
                        layoutHeight);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            } else if (position == 2) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

            }

        } else if (iamgeSize == 4) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        (layoutHeight - itemWhiteSpace) / 2);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            } else if (position == 2) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 3) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            }

        } else if (iamgeSize == 5) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace) / 2,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            } else if (position == 2) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 3) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            } else if (position == 4) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            }

        } else if (iamgeSize == 6) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            } else if (position == 2) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);

            } else if (position == 3) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 4) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            } else if (position == 5) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            }

        } else if (iamgeSize == 7) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            } else if (position == 2) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            } else if (position == 3) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 4) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.RIGHT_OF, imageViewList.get(3).getId());
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams1.leftMargin = itemWhiteSpace;
            } else if (position == 5) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.RIGHT_OF, imageViewList.get(4).getId());
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams1.leftMargin = itemWhiteSpace;
            } else if (position == 6) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            }

        } else if (iamgeSize == 8) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.RIGHT_OF, imageViewList.get(0).getId());
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams1.leftMargin = itemWhiteSpace;
            } else if (position == 2) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.RIGHT_OF, imageViewList.get(1).getId());
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams1.leftMargin = itemWhiteSpace;
            } else if (position == 3) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            } else if (position == 4) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 5) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.RIGHT_OF, imageViewList.get(4).getId());
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams1.leftMargin = itemWhiteSpace;
            } else if (position == 6) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.RIGHT_OF, imageViewList.get(5).getId());
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams1.leftMargin = itemWhiteSpace;
            } else if (position == 7) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 3) / 4,
                        (layoutHeight - itemWhiteSpace) / 2);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            }
        } else if (iamgeSize == 9) {
            if (position == 0) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 1) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            } else if (position == 2) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            } else if (position == 3) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.BELOW, imageViewList.get(0).getId());
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
                layoutParams1.topMargin = itemWhiteSpace;
            } else if (position == 4) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams1.topMargin = itemWhiteSpace;
                layoutParams1.addRule(RelativeLayout.BELOW, imageViewList.get(1).getId());
            } else if (position == 5) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams1.addRule(RelativeLayout.BELOW, imageViewList.get(2).getId());
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
                layoutParams1.topMargin = itemWhiteSpace;
            } else if (position == 6) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                layoutParams1.leftMargin = SizeUtils.dp2px(10);
            } else if (position == 7) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.CENTER_HORIZONTAL);
                layoutParams1.topMargin = itemWhiteSpace;
                layoutParams1.addRule(RelativeLayout.BELOW, imageViewList.get(4).getId());
            } else if (position == 8) {
                layoutParams1 = new RelativeLayout.LayoutParams(
                        (layoutWidth - itemWhiteSpace * 2) / 3,
                        (layoutHeight - itemWhiteSpace * 2) / 3);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//                layoutParams1.rightMargin = SizeUtils.dp2px(10);
            }
        }
        return layoutParams1;

    }


    /**
     * 显示图片
     */
    private void displayPhoto() {
        if (imageViewList == null || urlList == null) {
            return;
        }

        int urlSize = urlList.size();
        for (int i = 0; i < urlSize; i++) {
            if (urlSize == 1) {
                disPlayAll(0);
            } else if (urlSize == 2) {
                disPlayLeft(0);
                disPlayRight(1);

            } else if (urlSize == 3) {
                disPlayLeft(0);
                disPlayTopRight(1);
                disPlayBottomRight(2);

            } else if (urlSize == 4) {
                disPlayTopLeft(0);
                disPlayTopRight(1);
                disPlayBottomLeft(2);
                disPlayBottomRight(3);

            } else if (urlSize == 5) {
                disPlayTopLeft(0);
                disPlayTopRight(1);
                disPlayBottomLeft(2);
                disPlayNormal(3);
                disPlayBottomRight(4);

            } else if (urlSize == 6) {
                disPlayTopLeft(0);
                disPlayNormal(1);
                disPlayTopRight(2);
                disPlayBottomLeft(3);
                disPlayNormal(4);
                disPlayBottomRight(5);

            } else if (urlSize == 7) {
                disPlayTopLeft(0);
                disPlayNormal(1);
                disPlayTopRight(2);
                disPlayBottomLeft(3);
                disPlayNormal(4);
                disPlayNormal(5);
                disPlayBottomRight(6);

            } else if (urlSize == 8) {
                disPlayTopLeft(0);
                disPlayNormal(1);
                disPlayNormal(2);
                disPlayTopRight(3);
                disPlayBottomLeft(4);
                disPlayNormal(5);
                disPlayNormal(6);
                disPlayBottomRight(7);

            } else if (urlSize == 9) {
                disPlayTopLeft(0);
                disPlayNormal(1);
                disPlayTopRight(2);
                disPlayNormal(3);
                disPlayNormal(4);
                disPlayNormal(5);
                disPlayBottomLeft(6);
                disPlayNormal(7);
                disPlayBottomRight(8);

            }


        }
    }


    public void disPlayNormal(int position) {
        Glide.with(mContext)
                .load(urlList.get(position))
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                )
                .into(imageViewList.get(position));
    }

    public void disPlayAll(int position) {
        displayByGlide(position, multiAll);
//        Glide.with(mContext)
//                .load(urlList.get(position))
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .transform(multiAll)
//                )
//                .into(imageViewList.get(position));
    }

    public void disPlayTopLeft(int position) {
        displayByGlide(position, multiLeftTop);
//        Glide.with(mContext)
//                .load(urlList.get(position))
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .transform(multiLeftTop)
//                )
//                .into(imageViewList.get(position));
    }

    public void disPlayTopRight(int position) {
        displayByGlide(position, multiTopRight);
//        Glide.with(mContext)
//                .load(urlList.get(position))
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .transform(multiTopRight)
//                )
//                .into(imageViewList.get(position));
    }

    public void disPlayBottomRight(int position) {
        displayByGlide(position, multiBottomRight);
//        Glide.with(mContext)
//                .load(urlList.get(position))
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .transform(multiBottomRight)
//                )
//                .into(imageViewList.get(position));
    }

    public void disPlayBottomLeft(int position) {
        displayByGlide(position, multibottomLeft);
//        Glide.with(mContext)
//                .load(urlList.get(position))
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .transform(multibottomLeft)
//                )
//                .into(imageViewList.get(position));
    }

    public void disPlayLeft(int position) {
        displayByGlide(position, multiLeft);
//        Glide.with(mContext)
//                .load(urlList.get(position))
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .transform(multiLeft)
//                )
//                .into(imageViewList.get(position));
    }

    public void disPlayRight(int position) {
        displayByGlide(position, multiRight);
//        Glide.with(mContext)
//                .load(urlList.get(position))
//                .apply(new RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .transform(multiRight)
//                )
//                .into(imageViewList.get(position));
    }

    /**
     * 展示图片
     *
     * @param position
     * @param multiTransformation
     */
    public void displayByGlide(int position, MultiTransformation multiTransformation) {
        Glide.with(mContext)
                .load(urlList.get(position))
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(multiTransformation)
                        .placeholder(JfResourceUtil.getSkinDrawable(R.drawable.icon_wait_loading))
                )
                .into(imageViewList.get(position));
    }

    public interface onPhotoItmeClickListener {
        void photoItmeClick(int position, View view, String url, List<String> urlList);
    }


}
