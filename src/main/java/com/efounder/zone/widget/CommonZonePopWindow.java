package com.efounder.zone.widget;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;
import com.efounder.zone.bean.ZoneAddPopWindowBean;

import java.util.ArrayList;


/**
 * 点击+弹出的popupwindow
 *
 * @author
 */
public class CommonZonePopWindow extends PopupWindow {
    public LinearLayout conentView;
    LayoutInflater inflater;
    private Context context;
    private PopWindowItemOnClickListener listener;
    private ArrayList<ZoneAddPopWindowBean> beanList;
    private ArrayList<View> menuViewList;
    private  ImageView rightTopArrowView;

    public static CommonZonePopWindow getInstance(Activity activity) {
        final CommonZonePopWindow commonOtherPopWindow = new CommonZonePopWindow(activity);
        ArrayList<ZoneAddPopWindowBean> zoneAddPopWindowBeans = new ArrayList<>();

        zoneAddPopWindowBeans.add(new ZoneAddPopWindowBean(ResStringUtil.getString(R.string.zone_launch), R.drawable.pop_lunch, false, false));
        zoneAddPopWindowBeans.add(new ZoneAddPopWindowBean(ResStringUtil.getString(R.string.zone_all), R.drawable.pop_all, true, true));

        zoneAddPopWindowBeans.add(new ZoneAddPopWindowBean(ResStringUtil.getString(R.string.zone_friend), R.drawable.pop_friend, false, true));
        zoneAddPopWindowBeans.add(new ZoneAddPopWindowBean(ResStringUtil.getString(R.string.zone_follow), R.drawable.pop_focuse, false, true));
        zoneAddPopWindowBeans.add(new ZoneAddPopWindowBean(ResStringUtil.getString(R.string.zone_blacklist), R.drawable.pop_blacklist, false, false));
        commonOtherPopWindow.addMenuButton(zoneAddPopWindowBeans);
        return commonOtherPopWindow;
    }

    public CommonZonePopWindow(final Activity context) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        conentView = (LinearLayout) inflater.inflate(R.layout.popupwindow_common_group, null);
        this.context = context;

        // 设置SelectPicPopupWindow的View
        this.setContentView(conentView);
        // 设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        // 刷新状态
        this.update();
        // 实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0000000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
        this.setBackgroundDrawable(dw);

        // 设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.AnimationPreview);

        menuViewList = new ArrayList<>();

    }

    public void addMenuButton(ArrayList<ZoneAddPopWindowBean> beanList1) {
        this.beanList = beanList1;
        menuViewList.clear();
       // conentView.removeAllViews();
        for (int i = 0; i < beanList.size(); i++) {
            final ZoneAddPopWindowBean zoneAddPopWindowBean = beanList.get(i);
            View menuView = inflater.inflate(R.layout.popupwindow_common_group_item1, null);
            if (i == 0) {
                rightTopArrowView = (ImageView) menuView.findViewById(R.id.mobile_zone_top_arrow);
                rightTopArrowView.setVisibility(View.VISIBLE);
            }
            final ImageView imageView = menuView.findViewById(R.id.cb_menuCheckBox);
            imageView.setImageResource(zoneAddPopWindowBean.getDrawable());
            TextView menuNameView = menuView.findViewById(R.id.tv_menuname);
            //CheckBox checkBox = menuView.findViewById(R.id.cb_box);
            ImageView checkBox = menuView.findViewById(R.id.cb_box);
            if (zoneAddPopWindowBean.isChecked()) {
                //checkBox.setChecked(true);
                checkBox.setVisibility(View.VISIBLE);
            }

            menuNameView.setText(zoneAddPopWindowBean.getName());
            conentView.addView(menuView);
            menuViewList.add(menuView);
            final int position = i;
            menuView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view, position,beanList.get(position));
                    if (zoneAddPopWindowBean.isCanChecked() && !zoneAddPopWindowBean.isChecked()) {
                        //可以选中但是未选中，标记为选中
                        refreshState(position, zoneAddPopWindowBean);
                    }
                    dismiss();
                }
            });
        }
    }

    /**
     * 选中当前点击的，取消其他的选中
     * @param position
     * @param bean
     */
    private void refreshState(int position, ZoneAddPopWindowBean bean) {

        for (int i = 0; i < menuViewList.size(); i++) {
            View view = menuViewList.get(i);
            ImageView checkBox = view.findViewById(R.id.cb_box);
            //checkBox.setChecked(false);
            checkBox.setVisibility(View.INVISIBLE);
            beanList.get(i).setChecked(false);
            if (i == position) {
                checkBox.setVisibility(View.VISIBLE);
                //checkBox.setChecked(true);
            }
        }
        bean.setChecked(true);
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindow(View parent) {
        if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
            this.showAsDropDown(parent, 0, 0);
        } else {
            this.dismiss();
        }
    }

    public interface PopWindowItemOnClickListener {
        public void onClick(View view, int position,ZoneAddPopWindowBean bean);
    }

    public void setItemOnClickListener(PopWindowItemOnClickListener listener) {
        this.listener = listener;
    }
    public void hideRightArrow(){
        rightTopArrowView.setVisibility(View.GONE);
    }
}
