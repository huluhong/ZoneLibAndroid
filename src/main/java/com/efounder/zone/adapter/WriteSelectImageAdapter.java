package com.efounder.zone.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.view.SendImgProgressView;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.utils.JfResourceUtil;
import com.efounder.zone.R;
import com.efounder.zone.bean.ZoneCloudFile;

import java.util.List;

public class WriteSelectImageAdapter extends BaseAdapter {

    private Context context;
    private List<ZoneCloudFile> urls;
    private OnImgRemovedListener onImgRemovedListener;

    public WriteSelectImageAdapter(Context context, List<ZoneCloudFile> urls) {
        this.context = context;
        this.urls = urls;
    }

    @Override
    public int getCount() {
        return urls.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.mobile_zone_item_select_image, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.ivDelete = convertView.findViewById(R.id.iv_delete);
            viewHolder.ivImage = convertView.findViewById(R.id.iv_img);
            viewHolder.progressView = convertView.findViewById(R.id.progressView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position == urls.size()) {
            viewHolder.progressView.setVisibility(View.GONE);
            Drawable drawable1 = tintDrawable(JfResourceUtil.getSkinDrawable(R.drawable.mobile_zone_add_image), JfResourceUtil.getSkinColor(R.color.light_text_color));
//            viewHolder.ivImage.setImageDrawable(drawable1);
            GlideImageLoader.getInstance().displayImage(context, viewHolder.ivImage, drawable1);
            viewHolder.ivDelete.setVisibility(View.INVISIBLE);
        } else {
            LXGlideImageLoader.getInstance().displayImage(context, viewHolder.ivImage, urls.get(position).getCloudFilePath());
            viewHolder.ivDelete.setVisibility(View.VISIBLE);
            if (urls.get(position).getCloudProgress() >= 0 && urls.get(position).getCloudProgress() < 100) {
                viewHolder.progressView.setVisibility(View.VISIBLE);
                viewHolder.progressView.setProgress(urls.get(position).getCloudProgress());
            } else {
                viewHolder.progressView.setVisibility(View.GONE);
            }
        }
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position != urls.size() && onImgRemovedListener != null) {
                    onImgRemovedListener.onImgRemoved(position);
                }
            }
        });
        return convertView;
    }

    public class ViewHolder {
        ImageView ivImage;
        ImageView ivDelete;
        SendImgProgressView progressView;
    }

    public interface OnImgRemovedListener {
        void onImgRemoved(int position);
    }

    public void setOnImgRemovedListener(OnImgRemovedListener onImgRemovedListener) {
        this.onImgRemovedListener = onImgRemovedListener;
    }

    private Drawable tintDrawable(Drawable drawable, int colors) {
        //学习一下 newDrawable 和 mutate 的区别
        //drawable.mutate().setAlpha(150);才能使第二个半透明，第一个没有半透明。
        //问题3: Drawable.getConstantState().newDrawable()
        final Drawable wrappedDrawable = DrawableCompat.wrap(drawable).mutate();
        DrawableCompat.setTint(wrappedDrawable, colors);
        return wrappedDrawable;
    }
}
