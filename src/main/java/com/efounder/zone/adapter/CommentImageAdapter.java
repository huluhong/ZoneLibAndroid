package com.efounder.zone.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.zone.R;

import java.util.List;

public class CommentImageAdapter extends RecyclerView.Adapter<CommentImageAdapter.ViewHolder> {
    private Context context;
    private List<String> urls;
    private OnImgRemovedListener onImgRemovedListener;

    public CommentImageAdapter(Context context, List<String> urls) {
        this.context = context;
        this.urls = urls;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.mobile_zone_item_comment_image, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        LXGlideImageLoader.getInstance().displayImage(context, holder.ivImage, urls.get(position));

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onImgRemovedListener != null) {
                    onImgRemovedListener.onImgRemoved(position);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImage;
        ImageView ivDelete;
        public ViewHolder(View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_img);
            ivDelete = itemView.findViewById(R.id.iv_delete);
        }

    }

    public interface OnImgRemovedListener{
        void onImgRemoved(int position);
    }

    public void setOnImgRemovedListener(OnImgRemovedListener onImgRemovedListener) {
        this.onImgRemovedListener = onImgRemovedListener;
    }
}
