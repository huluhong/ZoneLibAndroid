package com.efounder.zone.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.zone.R;
import com.utilcode.util.ImageUtils;
import com.utilcode.util.SizeUtils;

import java.util.ArrayList;

public class PostSelectedImagesAdapter extends RecyclerView.Adapter<PostSelectedImagesAdapter.PicViewHolder> {

    private Context context;
    private ArrayList<String> mFiles;
    private OnPicDeletedListener onPicDeletedListener;

    // public static List<Object> eventAdapters= new ArrayList<>();

    public PostSelectedImagesAdapter(Context context, ArrayList<String> mFiles) {
        this.context = context;
        this.mFiles = mFiles;
    }

    @Override
    public PicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.mobile_zone_item_post_selected_image, parent, false);
        return new PicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PicViewHolder holder, int position) {

        holder.imageView.getLayoutParams().height = SizeUtils.dp2px(230);
        holder.imageView.getLayoutParams().width = Math.round(ImageUtils.getPicViewWidth(mFiles.get(position), SizeUtils.dp2px(230)));
        LXGlideImageLoader.getInstance().displayImage(context, holder.imageView, mFiles.get(position));

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onPicDeletedListener != null) {
                    onPicDeletedListener.onPicDeleted(holder.getAdapterPosition());
                }
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo 图片点击预览
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFiles.size();
    }

    class PicViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        ImageView ivDelete;

        public PicViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_preview);
            ivDelete = itemView.findViewById(R.id.iv_delete);
        }
    }

    public interface OnPicDeletedListener {
        void onPicDeleted(int position);
    }

    public void setOnPicDeletedListener(OnPicDeletedListener onPicDeletedListener) {
        this.onPicDeletedListener = onPicDeletedListener;
    }

}
