package com.efounder.zone.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.zone.R;

import java.util.ArrayList;

public class ImageHorizontalSlideSelectAdapter extends RecyclerView.Adapter<ImageHorizontalSlideSelectAdapter.PicViewHolder> {

    private Context context;
    private ArrayList<String> mFiles;

    public ImageHorizontalSlideSelectAdapter(Context context, ArrayList<String> mFiles) {
        this.context = context;
        this.mFiles = mFiles;
    }

    @Override
    public PicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.mobile_zone_item_image_horizontal_slide_select, parent, false);
        return new PicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PicViewHolder holder, int position) {

        LXGlideImageLoader.getInstance().displayImage(context, holder.imageView, mFiles.get(position));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo 图片点击发送
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFiles.size();
    }

    class PicViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public PicViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_preview);
        }
    }


}
