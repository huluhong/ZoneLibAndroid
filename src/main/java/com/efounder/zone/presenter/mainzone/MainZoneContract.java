package com.efounder.zone.presenter.mainzone;

import com.efounder.frame.baseui.BaseContract;

/**
 * 空间主界面
 * @author YQS
 */
public interface MainZoneContract {

    interface MainZonePresenter extends BaseContract.BasePresenter{
        void getNetData(boolean isSelf,String time );


    }

    //公众号列表的view 接口
    interface MainZoneView extends BaseContract.BaseView<MainZonePresenter> {

    }
}
