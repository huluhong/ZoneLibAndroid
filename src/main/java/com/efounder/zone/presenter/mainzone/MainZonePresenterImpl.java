//package com.efounder.zone.presenter.mainzone;
//
//import android.os.Handler;
//
//import com.efounder.chat.event.SystemInitOverEvent;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.frame.baseui.BasePresenterImpl;
//import com.efounder.frame.utils.Constants;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.zone.bean.TalksBean;
//import com.efounder.zone.util.ZoneRequestUtil;
//import com.efounder.zone.widget.ThumbsCommentsTransmitView;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import com.utilcode.util.ToastUtils;
//
//import org.greenrobot.eventbus.EventBus;
//import org.json.JSONArray;
//
//import java.util.HashMap;
//import java.util.Random;
//
//import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//
//
//public class MainZonePresenterImpl extends BasePresenterImpl<MainZoneContract.MainZoneView>
//        implements MainZoneContract.MainZonePresenter {
//
//    private static final String TAG = "MainZonePresenterImpl";
//    public MainZonePresenterImpl(MainZoneContract.MainZoneView view) {
//        super(view);
//    }
//
//
//    @Override
//    public void getNetData(boolean isSelf ,String lastTime) {
//        HashMap<String, String> params = new HashMap<>();
//        if (isSelf) {
//            //password
//            params.put("password", EnvironmentVariable.getProperty(Constants.CHAT_PASSWORD));
//        }
//        params.put("time", lastTime);
//        //自己的userId
//        params.put("userid", EnvironmentVariable.getProperty(CHAT_USER_ID));
//        params.put("password", EnvironmentVariable.getProperty(CHAT_PASSWORD));
//
//        ZoneRequestUtil.getCommonRequest(TAG, isSelf ? "events/" + dataOrigin : "shortposts/" + userId + "/u", params, new ZoneRequestUtil.ZoneRequestCallback() {
//            @Override
//            public void onSuccess(String response) {
//                LoadingDataUtilBlack.dismiss();
//                //更新EV，刷新角标
//                EnvironmentVariable.setProperty("zone_has_new_post", "false");
//                EventBus.getDefault().post(new SystemInitOverEvent());
//
//                try {
//                    JSONArray jsonArray = new JSONArray(response);
//
//                    if (isRefresh) {
//                        refreshLayout.finishRefresh(100);
//                        //清空数据，只保留第一条头部数据
//                        //请求到数据，则清空原数据，没有数据则不清空数据
////                        if (jsonArray.length() > 0) {
//                        if (items.size() > 1) {
//                            TalksBean talksBean = items.get(0);
//                            items.clear();
//                            items.add(talksBean);
//                        }
//                        if (isSelf) {
//                            // 根据EV，增加项目
//                            String hasNewMessage = EnvironmentVariable.getProperty("zone_has_new_message");
//                            if ("true".equals(hasNewMessage) || "false".equals(hasNewMessage)) {
//                                hasNewMessage = "0";
//                                EnvironmentVariable.setProperty("zone_has_new_message", "0");
//                            }
//                            if (hasNewMessage != null && !hasNewMessage.equals("") && !hasNewMessage.equals("0")) {
//                                int count = Integer.valueOf(hasNewMessage);
//                                TalksBean talksBean = new TalksBean();
//                                talksBean.setObjType(0);
//                                TalksBean.ObjBean objBean = new TalksBean.ObjBean();
//                                objBean.setLikeCount(count);
//                                talksBean.setObj(objBean);
//                                items.add(talksBean);
//                            }
//                        }
//
//                    } else {
//                        if (jsonArray.length() == 0) {
//                            ToastUtils.showShort("没有更多动态了");
//                        }
//                        refreshLayout.finishLoadMore(100);
//                    }
//
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        Gson gson = new GsonBuilder().serializeNulls().create();
//                        TalksBean talksBean = gson.fromJson(jsonArray.getString(i), TalksBean.class);
//                        //相册类型数据，过滤掉
//                        if (talksBean.getObjType() != 5) {
//                            if (talksBean.getObj() == null) {
//                                continue;
//                            }
//                            //todo 浏览次数修改
//                            int count = talksBean.getObj().getReadCount()
//                                    * ThumbsCommentsTransmitView.MULTI + new Random().nextInt(99);
//                            talksBean.getObj().setReadCount(count);
//                            items.add(talksBean);
//                            //最后一条状态的时间，用于请求更多
//                            lastTalksTime = talksBean.getObj().getPostCreatetime();
//                        }
//                    }
//                    adapter.notifyDataSetChanged();
//                    if (isRefresh) {
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                recyclerView.scrollToPosition(0);
//                            }
//                        }, 1000);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    if (isRefresh) {
//                        refreshLayout.finishRefresh(100);
//                    } else {
//                        refreshLayout.finishLoadMore(100);
//                    }
//                }
//            }
//
//            @Override
//            public void onFail(String error) {
//                LoadingDataUtilBlack.dismiss();
//                if (isRefresh) {
//                    TalksBean talksBean = items.get(0);
//                    items.clear();
//                    items.add(talksBean);
//                    adapter.notifyDataSetChanged();
//                    refreshLayout.finishRefresh(100);
//                } else {
//                    refreshLayout.finishLoadMore(100);
//                }
//            }
//        });
//    }
//}
