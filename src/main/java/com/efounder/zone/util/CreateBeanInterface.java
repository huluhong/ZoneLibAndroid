package com.efounder.zone.util;

import com.efounder.zone.bean.TalksBean;

import java.util.List;

public interface CreateBeanInterface {

    /**
     * 发说说时创建的talksBean
     * @param type 消息类型 0：日志 2：说说
     * @param device 发送动态的设备名称
     * @param content 消息内容
     * @return 返回的动态内容
     */
    TalksBean createTalksBean(int type, String device, String content);

    /**
     * 转发说说
     * @param device 发送动态的设备名称
     * @param content 消息内容
     * @return
     */
    TalksBean createTransmitTalksBean(TalksBean talksBean,String device, String content);
    /**
     * 创建普通说说的内容 type = 1, 纯文本； type = 2, 文本（可为“”）+图片
     * @param text 文本
     * @param images 图片地址list
     * @return 说说内容， “{ }”
     */
    String createCommonContent(String text, List<String> images);

    /**
     * 创建单张图片说说的内容 type = 2, 文本（可为“”）+图片 + width + height
     * @param text 文本
     * @param images 图片地址list
     * @return 说说内容， “{ }”
     * @param height 图片高度
     * @param width 图片宽度
     */
    String createSingleImageContent(String text, List<String> images, int width, int height);

    /**
     * 创建网页说说的内容 type = 4,  文本（可为“”）+网页
     * @param text 文本
     * @param url 网页地址高度
     * @param height webView
     * @return 说说内容， “{ }”
     */
    String createWebContent(String text, String url, int height);

    /**
     * 创建视频说说的内容 type = 3,  文本（可为“”）+视频
     * @param text 文本
     * @param url 视频地址
     * @return 说说内容， “{ }”
     */
    String createVideoContent(String text, String url);

    /**
     * 创建动态评论bean
     * @param commentObjectId 评论动态id
     * @param commentObjectType 评论动态类型
     * @param comment 评论内容
     * @return CommentBean
     */
    TalksBean.CommentBean createNewTalksCommentBean(int commentObjectId, int commentObjectType,
              String comment);

    /**
     * 创建回复评论bean
     * @param commentObjectId 评论动态id
     * @param commentObjectType 评论动态类型
     * @param comment 评论内容
     * @param commentParent 回复评论的id
     * @param replyUserId 回复的评论用户id
     * @return ChildCommentBean
     */
    TalksBean.CommentBean.ChildCommentsBean createReplyCommentBean(int commentObjectId, int commentObjectType,
              int commentParent, int replyUserId, String comment);

    /**
     * 创建评论内容
     * @param text 评论文本
     * @param images 评论图片
     * @return 评论内容
     */
    String createComment(String text, List<String> images);

    /**
     * 点赞时传递的likesBean,用于刷新界面
     * @return likesBean
     */
    TalksBean.LikesBean crateNewLikesBean();

}
