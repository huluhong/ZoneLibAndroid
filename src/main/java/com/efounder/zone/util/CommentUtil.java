package com.efounder.zone.util;

import android.content.Context;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.Constants;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;
import com.efounder.zone.bean.TalksBean;
import com.efounder.zone.commentview.ZoneCommentBean;
import com.efounder.zone.event.MobileZoneEvent;
import com.efounder.zone.event.MobileZoneRefreshEvent;
import com.efounder.zone.fragment.CommentInputDialogFragment;
import com.efounder.zone.fragment.TransmitDialogFragment;
import com.efounder.zone.widget.StatusAlert;

import com.utilcode.util.ActivityUtils;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import static android.app.DialogFragment.STYLE_NO_FRAME;

/**
 * Created by Richard on 2018/5/11.
 */

public class CommentUtil {


    /**
     * 发送评论
     *
     * @param position    动态, 如果position == 0, 约定为从消息界面回复的评论
     * @param commentBean 回复的评论
     */
    public static void sendComment(Context context, int position, ZoneCommentBean commentBean, TalksBean talksBean, FragmentManager fragmentManager) {

        CommentInputDialogFragment commentInputDialogFragment = new
                CommentInputDialogFragment();
        commentInputDialogFragment.setParams(context, position, talksBean, commentBean);
        commentInputDialogFragment.setStyle(STYLE_NO_FRAME, 0);
        commentInputDialogFragment.show(fragmentManager, "CommentInput");
    }

    /**
     * 转发评论
     *
     * @param context
     * @param talksBean
     * @param fragmentManager
     */

    public static void transmitContent(Context context, TalksBean talksBean, FragmentManager fragmentManager) {
        TransmitDialogFragment transmitDialogFragment = new TransmitDialogFragment();
        transmitDialogFragment.setParams(context, talksBean);
        transmitDialogFragment.setStyle(STYLE_NO_FRAME, 0);
        transmitDialogFragment.show(fragmentManager, "TransmitContent");
    }

    /**
     * 删除自己的评论
     *
     * @param position  该条评论所在动态
     * @param commentId 该条评论id
     */
    public static void deleteComment(final Context context, final int position, final TalksBean talksBean, final int commentId) {
        String method = "comments/" + commentId;
        HashMap<String, String> params = new HashMap<>();
        params.put("userid", String.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
        LoadingDataUtilBlack.show(ActivityUtils.getTopActivity(),ResStringUtil.getString(R.string.common_text_please_wait));
        ZoneRequestUtil.postCommonRequest("MainZoneActivity", method, params, new ZoneRequestUtil.ZoneRequestCallback() {
            @Override
            public void onSuccess(String response) {
                LoadingDataUtilBlack.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("status").equals("104001")) {
                        StatusAlert.successAlert(context, LayoutInflater.from(context)
                                .inflate(R.layout.mobile_zone_activity_main_zone, null), ResStringUtil.getString(R.string.zone_delete_success));
                        List<TalksBean.CommentBean> commentBeanList = talksBean.getComment();
                        for (int i = 0; i < commentBeanList.size(); i++) {
                            if (commentBeanList.get(i).getId() == commentId) {
                                commentBeanList.remove(i);
                            }
                        }
                        //todo 设置积分
                        int integral=0;
                        try {
                            integral = jsonObject.optInt("integral",talksBean.getObj().getIntegral());
                            talksBean.getObj().setIntegral(integral);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new MobileZoneEvent(position));
                        EventBus.getDefault().post(new MobileZoneRefreshEvent(talksBean, null));
                    } else {
                        StatusAlert.failAlert(context, LayoutInflater.from(context)
                                .inflate(R.layout.mobile_zone_activity_main_zone, null), ResStringUtil.getString(R.string.zone_delete_fail));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    StatusAlert.failAlert(context, LayoutInflater.from(context)
                            .inflate(R.layout.mobile_zone_activity_main_zone, null), ResStringUtil.getString(R.string.zone_delete_fail));
                }


            }

            @Override
            public void onFail(String error) {
                LoadingDataUtilBlack.dismiss();
                StatusAlert.failAlert(context, LayoutInflater.from(context)
                        .inflate(R.layout.mobile_zone_activity_main_zone, null), ResStringUtil.getString(R.string.zone_delete_fail));
            }
        });
    }

//    /**
//     * 取消评论输入框
//     */
//    public static void cancelCommentInput() {
//        if (commentInputDialogFragment != null && commentInputDialogFragment.getDialog() != null) {
//            commentInputDialogFragment.getDialog().dismiss();
//            commentInputDialogFragment = null;
//        }
//
//    }


}
