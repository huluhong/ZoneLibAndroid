package com.efounder.zone.util;

import android.content.Context;
import android.os.Bundle;

import com.efounder.chat.db.WeChatDBManager;
import com.efounder.frame.activity.EFTransformFragmentActivity;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.frame.utils.EFFrameUtils;
import com.efounder.mobilecomps.contacts.User;

/**
 * 空间跳转等的帮助类
 * @author will
 */
public class ZoneHelperUtil {

    /**
     * 跳转到空间
     * @param userId 判断id，是好友，跳好友空间，否则进个人详情
     */
    public static void gotoOtherZone(Context context, int userId) {
        User user = WeChatDBManager.getInstance().getOneFriendById(userId);
        //是好友，进入空间
       // if (user.isExist()) {
            try {
                Bundle bundle = new Bundle();
                bundle.putInt("other_user_id", userId);
                bundle.putBoolean(EFTransformFragmentActivity.EXTRA_HIDE_TITLE_BAR, true);
                EFFrameUtils.pushFragment((Class<? extends BaseFragment>) Class.forName("com.efounder.zone.fragment.MainZoneFragment"), bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
//        } else {
//            //不是好友，跳个人信息页面
//            try {
//                Intent intent = new Intent();
//                //通过主工程配置 可以获取到要跳转的界面
//                String className =context.getResources().getString(R.string.userinfo_actiivity_class);
//                Class clazz = Class.forName(className);
//                intent.setClass(context,clazz);
//                intent.putExtra("id", userId);
//                context.startActivity(intent);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

    }
}
