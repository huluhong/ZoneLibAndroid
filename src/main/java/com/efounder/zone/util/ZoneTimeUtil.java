package com.efounder.zone.util;


import com.efounder.frame.language.MultiLanguageUtil;
import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 空间时间处理工具
 */
public class ZoneTimeUtil {
    private static final DateFormat DEFAULT_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat DEFAULT_EN_FORMAT = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    private static final DateFormat DEFAULT_FORMAT_HOUR = new SimpleDateFormat("HH:mm");

    private static final DateFormat DEFAULT_FORMAT_WITHOUTTIME = new SimpleDateFormat("yyyy-MM-dd 00:00:00");

    /**
     * 返回处理过的时间
     *
     * @param time long类型的时间转string
     */
    public static String formatTime(String time) {
        String dateString = time;
        // 获取系统当前时间
        Date now = new Date();
        try {
            long currentTime = now.getTime();
            long dataTime = Long.valueOf(time);

            //今天已经过去的时间
            long todayPastTime = currentTime - getStartTime().getTime();
            //今天过去了几个小时
            int pastHour = (int) (todayPastTime / (60 * 60 * 1000));

            // 计算两个时间点相差的秒数
            long seconds = (currentTime - dataTime);
            if (seconds < 10 * 1000) {
                dateString = ResStringUtil.getString(R.string.zone_just);
            } else if (seconds < 60 * 1000) {
                dateString = seconds / 1000 + ResStringUtil.getString(R.string.zone_second_before);
            } else if (seconds < 60 * 60 * 1000) {
                dateString = seconds / 1000 / 60 + ResStringUtil.getString(R.string.zone_minute_before);
            } else if (seconds < 60 * 60 * pastHour * 1000) {
                dateString = seconds / 1000 / 60 / 60 + ResStringUtil.getString(R.string.zone_hour_before);
            } else if (dataTime > getBeforeYesterDay().getTime() && dataTime < getYesterDay().getTime()) {
                dateString = ResStringUtil.getString(R.string.zone_before_yesterday) + DEFAULT_FORMAT_HOUR.format(new Date(dataTime));
            } else if (dataTime > getYesterDay().getTime() && dataTime <= getEndTime(-1).getTime()) {
                dateString = ResStringUtil.getString(R.string.zone_yesterday) + DEFAULT_FORMAT_HOUR.format(new Date(dataTime));
            } else {
                if(MultiLanguageUtil.getInstance().getSetLanguageLocale().getCountry().equals("CN")){
                    dateString = DEFAULT_FORMAT.format(dataTime);
                }else {
                    dateString = DEFAULT_EN_FORMAT.format(dataTime);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateString;

    }

    /**
     * 获取一天的起始时间
     *
     * @return
     */
    public static Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    /**
     * 获取一天的结束时间
     *
     * @return
     */
    public static Date getnowEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

    /**
     * 获取一天的结束时间
     *
     * @param offSet 偏移量 0 今天 -1 昨天
     * @return
     */
    public static Date getEndTime(int offSet) {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.add(Calendar.DAY_OF_MONTH, offSet);
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

    /**
     * 获取昨天时间
     *
     * @return 2018-04-26 00:00:00
     */
    public static Date getYesterDay() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.add(Calendar.DAY_OF_MONTH, -1);
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    /**
     * 获取前天时间
     *
     * @return 2018-04-26 00:00:00
     */
    public static Date getBeforeYesterDay() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.add(Calendar.DAY_OF_MONTH, -2);
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

}
