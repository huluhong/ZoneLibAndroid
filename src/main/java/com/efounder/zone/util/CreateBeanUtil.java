package com.efounder.zone.util;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.Constants;
import com.efounder.zone.bean.TalksBean;
import com.utilcode.util.TimeUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CreateBeanUtil implements CreateBeanInterface {

    public CreateBeanUtil() {
    }

    @Override
    public TalksBean createTalksBean(int type, String device, String content) {
        TalksBean talksBean = new TalksBean();
        talksBean.setObjType(type);
        TalksBean.ObjBean objBean = new TalksBean.ObjBean();
        objBean.setDevice(device);
        objBean.setPostContent(content);
        objBean.setPostCreatetime(String.valueOf(TimeUtils.getNowMills()));
        objBean.setPostAuthor(Integer.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
        talksBean.setObj(objBean);
        talksBean.setLikes(new ArrayList<TalksBean.LikesBean>());
        talksBean.setComment(new ArrayList<TalksBean.CommentBean>());
        return talksBean;
    }

    @Override
    public TalksBean createTransmitTalksBean(TalksBean talksBean,String device, String content) {
        TalksBean.ObjBean objBean = talksBean.getObj();
        TalksBean.ObjBean transmitObj = new TalksBean.ObjBean();
        transmitObj.setDevice(device);
        transmitObj.setPostContent(objBean.getPostContent());
        transmitObj.setPostCreatetime(String.valueOf(TimeUtils.getNowMills()));
        if(objBean.getTransmitStatus() == 1){
            transmitObj.setTransmitSourceId(objBean.getTransmitSourceId());
        }else {
            transmitObj.setTransmitSourceId(objBean.getId());

        }
//        objBean.setId();
        transmitObj.setTransmitStatus(1);
//        objBean.setTransmitSourceId(objBean.getId());
        if (objBean.getTransmitSourceUserid() != null) {
            //转发的转发
            transmitObj.setTransmitSourceUserid(objBean.getTransmitSourceUserid());
        }else {
            //转发正常说说
            transmitObj.setTransmitSourceUserid(objBean.getPostAuthor());
        }
        transmitObj.setReadCount(0);
        transmitObj.setTransmitContent(content);
        transmitObj.setPostAuthor(Integer.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
        TalksBean transmitTalksBean = new TalksBean();
        transmitTalksBean.setObj(transmitObj);
        transmitTalksBean.setObjType(talksBean.getObjType());
        transmitTalksBean.setLikes(new ArrayList<TalksBean.LikesBean>());
        transmitTalksBean.setComment(new ArrayList<TalksBean.CommentBean>());
        return transmitTalksBean;
    }

    @Override
    public String createCommonContent(String text, List<String> images) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("images", new JSONArray(images));
        content.put("text", text);
        content.put("type", images.size() == 0 ? 1 : 2);
        return String.format("%s", new JSONObject(content).toString());
    }

    @Override
    public String createSingleImageContent(String text, List<String> images, int width, int height) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("images", new JSONArray(images));
        content.put("text", text);
        content.put("type", 2);
        content.put("width", width);
        content.put("height", height);
        return String.format("%s", new JSONObject(content).toString());
    }

    @Override
    public String createWebContent(String text, String url, int height) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("text", text);
        content.put("type", 4);
        HashMap<String, Object> urlMap = new HashMap<>();
        urlMap.put("url", url);
        urlMap.put("height", height);
        content.put("web", new JSONObject(urlMap));
        return String.format("%s", new JSONObject(content).toString());
    }

    @Override
    public String createVideoContent(String text, String url) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("text", text);
        content.put("type", 3);
        content.put("video", url);
        return String.format("%s", new JSONObject(content).toString());
    }

    @Override
    public TalksBean.CommentBean createNewTalksCommentBean(int commentObjectId, int commentObjectType, String comment) {
        TalksBean.CommentBean commentBean = new TalksBean.CommentBean();
        commentBean.setObjectId(commentObjectId);
        commentBean.setObjectType(commentObjectType);
        commentBean.setContent(comment);
        commentBean.setCommentUserId(Integer.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
        return commentBean;
    }

    @Override
    public TalksBean.CommentBean.ChildCommentsBean createReplyCommentBean(int commentObjectId, int commentObjectType, int commentParent, int replyUserId, String comment) {
        TalksBean.CommentBean.ChildCommentsBean commentBean = new TalksBean.CommentBean.ChildCommentsBean();
        commentBean.setObjectId(commentObjectId);
        commentBean.setObjectType(commentObjectType);
        commentBean.setParentId(commentParent);
        commentBean.setReplyUserId(replyUserId);
        commentBean.setCommentUserId(Integer.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
        commentBean.setContent(comment);
        return commentBean;
    }

    @Override
    public String createComment(String text, List<String> images) {
        final HashMap<String, Object> commentContent = new HashMap<>();
        //图片评论
        commentContent.put("images", new JSONArray(images));
        //文字评论
        commentContent.put("text", text);
        return String.format("%s", new JSONObject(commentContent).toString());
    }

    @Override
    public TalksBean.LikesBean crateNewLikesBean() {
        TalksBean.LikesBean likesBean = new TalksBean.LikesBean();
        likesBean.setUserId(Integer.valueOf(EnvironmentVariable.getProperty(Constants.CHAT_USER_ID)));
        return likesBean;
    }
}
