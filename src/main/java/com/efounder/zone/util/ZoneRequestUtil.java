package com.efounder.zone.util;

import android.util.Log;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.http.EFHttpRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

public class ZoneRequestUtil {

    public static final String BASE_URL = EnvironmentVariable.getProperty("OSPMZOneURL");

    private static final String TAG = "ZoneRequestUtil";

    /**
     * 通用get请求
     *
     * @param tag       tag
     * @param method    url: 传入"path/method"
     * @param paramsMap 参数
     * @param callback  callback
     */
    public static void getCommonRequest(String tag, String method, HashMap<String, String> paramsMap, final ZoneRequestCallback callback) {
        StringBuilder tempParams = new StringBuilder();
        try {
            int pos = 0;
            for (String key : paramsMap.keySet()) {
                if (pos > 0) {
                    tempParams.append("&");
                }
                tempParams.append(String.format("%s=%s", key, URLEncoder.encode(paramsMap.get(key), "utf-8")));
                pos++;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String requestUrl = String.format("%s%s?%s", BASE_URL, method, tempParams.toString());
        EFHttpRequest httpRequest = new EFHttpRequest(tag);
        httpRequest.setHttpRequestListener(new EFHttpRequest.HttpRequestListener() {
            @Override
            public void onRequestSuccess(int requestCode, String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onRequestFail(int requestCode, String message) {
                callback.onFail(message);
            }
        });
        Log.d("ZoneGetRequest", requestUrl);
        httpRequest.httpGet(requestUrl, 30000);
    }

    /**
     * 通用post方法请求
     *
     * @param tag       class name
     * @param method    url: 传入"path/method"
     * @param paramsMap 参数
     * @param callback  callback
     */
    public static void postCommonRequest(final String tag, String method, HashMap<String, String> paramsMap, final ZoneRequestCallback callback) {
        EFHttpRequest httpRequest = new EFHttpRequest(tag);
        httpRequest.setHttpRequestListener(new EFHttpRequest.HttpRequestListener() {
            @Override
            public void onRequestSuccess(int requestCode, String response) {
                if (callback != null) {
                    if (response != null)
                        Log.i(tag, response);
                    callback.onSuccess(response);
                }
            }

            @Override
            public void onRequestFail(int requestCode, String message) {
                if (callback != null) {
                    if (message != null)
                        Log.e(tag, message);
                    callback.onFail(message);
                }
            }
        });
        httpRequest.httpPost(String.format("%s%s", BASE_URL, method), paramsMap);
    }

    /**
     * 简单的get请求，没有参数
     *
     * @param tag       tag
     * @param method    url: 传入"path/method"
     * @param callback  callback
     */
    public static void getSimpleRequest(String tag, String method, final ZoneRequestCallback callback) {

        String requestUrl = String.format("%s%s", BASE_URL, method);
        EFHttpRequest httpRequest = new EFHttpRequest(tag);
        httpRequest.setHttpRequestListener(new EFHttpRequest.HttpRequestListener() {
            @Override
            public void onRequestSuccess(int requestCode, String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onRequestFail(int requestCode, String message) {
                callback.onFail(message);
            }
        });
        Log.d("ZoneGetRequest", requestUrl);
        httpRequest.httpGet(requestUrl, 30000);
    }

    public interface ZoneRequestCallback {

        void onSuccess(String response);

        void onFail(String error);

    }
}
