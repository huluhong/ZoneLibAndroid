package com.efounder.zone.commentview;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.efounder.chat.activity.ViewPagerActivity;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.widget.RoundImageView;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.zone.R;
import com.efounder.zone.bean.TalksBean;
import com.utilcode.util.TimeUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.photopicker.widget.BGANinePhotoLayout;

/**
 * 动态详情界面的
 * Created by slp on 2018/5/15.
 */
public class DetailCommentListAdapter extends RecyclerView.Adapter<DetailCommentListAdapter.DetailViewHolder> implements BGANinePhotoLayout.Delegate {

    private List<ZoneCommentBean> mCommentBeans;
    private Context mContext;
    private WeChatDBManager mWeChatDBManager;
    private OnDetailCommentListener mListener;

    public DetailCommentListAdapter(List<ZoneCommentBean> commentBeanList, Context context, OnDetailCommentListener listener) {
        this.mCommentBeans = commentBeanList;
        this.mContext = context;
        this.mListener = listener;
        this.mWeChatDBManager = WeChatDBManager.getInstance();

    }

    @Override
    public DetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.mobile_zone_item_detail_comment, parent, false);
        return new DetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailViewHolder holder, int position) {
        final ZoneCommentBean commentBean = mCommentBeans.get(position);
        String content = commentBean.getContent();

        final int userId = commentBean.getCommentUserId();
        User user = mWeChatDBManager.getOneUserById(userId);
        if (!user.isExist()){
            user.setNickName((String) commentBean.getCommentUserNickname());
            user.setAvatar(commentBean.getCommentUserAvatar()==null? "": (String) commentBean.getCommentUserAvatar());
        }


        holder.tvName.setText(user.getNickName());
        LXGlideImageLoader.getInstance().showUserAvatar(mContext, holder.ivAvatar, user.getAvatar());
        if (commentBean.getCreatetime() != null){
            holder.tvTime.setText(TimeUtils.millis2String(Long.valueOf(commentBean.getCreatetime())));
        }else {
            holder.tvTime.setText(TimeUtils.millis2String(Long.valueOf(System.currentTimeMillis())));

        }

        //判断content中是否为json格式
        try {
            JSONObject jsonObject = new JSONObject(content);

            if (jsonObject.has("text")) {
                String commentText = jsonObject.getString("text");
                if ("".equals(commentText)) {
                    holder.tvContent.setVisibility(View.GONE);
                }
                holder.tvContent.setText(commentText);
            }
            if (jsonObject.has("images")) {
                JSONArray jsonArray = jsonObject.getJSONArray("images");
                ArrayList<String> imgList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    imgList.add(jsonArray.getString(i));
                }
                if (imgList.size() <= 0) {
                    holder.bplDetailCommentPhoto.setVisibility(View.GONE);
                } else {
                    holder.bplDetailCommentPhoto.setVisibility(View.VISIBLE);
                    holder.bplDetailCommentPhoto.setData(imgList);
                    holder.bplDetailCommentPhoto.setDelegate(DetailCommentListAdapter.this);
                }
            }

        } catch (Exception e) {
            holder.bplDetailCommentPhoto.setVisibility(View.GONE);
            String commentText = content;
            holder.tvContent.setText(commentText);
        }
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onNameClick(String.valueOf(userId));
                }
            }
        });
        holder.ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onNameClick(String.valueOf(userId));
                }
            }
        });
        holder.tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onCommentClick(commentBean);
                }
            }
        });
        holder.rlDetailComment.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        //重新处理数据
        if(commentBean.getChildComments()!= null){
            List<ZoneCommentBean> childComment = new ArrayList<>();
            for (TalksBean.CommentBean.ChildCommentsBean childCommentsBean:(commentBean.getChildComments())) {
                ZoneCommentBean zoneCommentBean = new ZoneCommentBean();

                zoneCommentBean.setId(childCommentsBean.getId());
                zoneCommentBean.setCreatetime(childCommentsBean.getCreatetime());
                zoneCommentBean.setContent(childCommentsBean.getContent());
                zoneCommentBean.setParentId(childCommentsBean.getParentId());
//                zoneCommentBean.setChildComments(childCommentsBean.getChildComments());
                zoneCommentBean.setCommentUserAvatar(childCommentsBean.getCommentUserAvatar());
                zoneCommentBean.setObjectId(childCommentsBean.getObjectId());
                zoneCommentBean.setCommentUserNickname(childCommentsBean.getCommentUserNickname());
                zoneCommentBean.setReplyUserId(childCommentsBean.getReplyUserId());
                zoneCommentBean.setReplyUserAvatar(childCommentsBean.getReplyUserAvatar());
                zoneCommentBean.setCommentUserId(childCommentsBean.getCommentUserId());
                zoneCommentBean.setObjectType(childCommentsBean.getObjectType());
                zoneCommentBean.setReplyUserNickname(childCommentsBean.getReplyUserNickname());

                childComment.add(zoneCommentBean);
            }
            ChildCommentListAdapter childCommentAdapter = new ChildCommentListAdapter(childComment,
                    mContext, new ChildCommentListAdapter.onCommentListener() {
                @Override
                public void onUserClick(String userId) {
                    if (mListener != null) {
                        mListener.onNameClick(String.valueOf(userId));
                    }
                }

                @Override
                public void onCommentClick(ZoneCommentBean childCommentsBean) {
                    if (mListener != null) {
                        mListener.onCommentClick(childCommentsBean);
                    }
                }
            });
            holder.rlDetailComment.setAdapter(childCommentAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return mCommentBeans.size();
    }

    @Override
    public void onClickNinePhotoItem(BGANinePhotoLayout ninePhotoLayout, View view, int position, String model, List<String> models) {
        String[] strings = models.toArray(new String[0]);
        ViewPagerActivity.showImageWall(mContext, strings, position, view);
    }

    public class DetailViewHolder extends RecyclerView.ViewHolder {

        private RoundImageView ivAvatar;
        private TextView tvName;
        private TextView tvTime;
        private TextView tvContent;
        private BGANinePhotoLayout bplDetailCommentPhoto;

        private RecyclerView rlDetailComment;

        public DetailViewHolder(View itemView) {
            super(itemView);

            ivAvatar = itemView.findViewById(R.id.riv_detail_comment_avatar);
            tvName = itemView.findViewById(R.id.tv_detail_comment_nickname);
            tvTime = itemView.findViewById(R.id.tv_detail_comment_time);
            tvContent = itemView.findViewById(R.id.tv_detail_comment_content);
            bplDetailCommentPhoto = itemView.findViewById(R.id.bpl_detail_comment_photo);
            rlDetailComment = itemView.findViewById(R.id.rv_child_comment);
        }
    }

    public interface OnDetailCommentListener {

        /**
         * 点击评论人的姓名
         * @param userId 评论人Id
         */
        void onNameClick(String userId);

        /**
         * 评论的内容
         * @param commentBean 评论的bean
         */
        void onCommentClick(ZoneCommentBean commentBean);
    }
}
