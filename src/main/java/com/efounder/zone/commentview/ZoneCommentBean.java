package com.efounder.zone.commentview;

import com.efounder.zone.bean.TalksBean;

import java.util.List;

/**
 * Created by slp on 2018/5/9.
 */
public class ZoneCommentBean {

    /**
     * id : 3139
     * createtime : 1525311898000
     * content : {"text":"1","images":[]}
     * parentId : null
     * childComments : [{"id":3175,"createtime":"1525851579000","content":"111","parentId":3139,"childComments":null,"commentUserAvatar":null,"objectId":2854,"commentUserNickname":null,"replyUserId":2369,"replyUserAvatar":null,"commentUserId":2982,"objectType":2,"replyUserNickname":null},{"id":3176,"createtime":"1525851760000","content":"6666","parentId":3139,"childComments":null,"commentUserAvatar":null,"objectId":2854,"commentUserNickname":null,"replyUserId":2982,"replyUserAvatar":null,"commentUserId":2920,"objectType":2,"replyUserNickname":null},{"id":3177,"createtime":"1525851862000","content":"0344","parentId":3139,"childComments":null,"commentUserAvatar":null,"objectId":2854,"commentUserNickname":null,"replyUserId":2982,"replyUserAvatar":null,"commentUserId":2920,"objectType":2,"replyUserNickname":null}]
     * commentUserAvatar : null
     * objectId : 2854
     * commentUserNickname : null
     * replyUserId : null
     * replyUserAvatar : null
     * commentUserId : 2369
     * objectType : 2
     * replyUserNickname : null
     */

    private int id;
    private String createtime;
    private String content;
    private Object parentId;
    private Object commentUserAvatar;
    private int objectId;
    private Object commentUserNickname;
    private Object replyUserId;
    private Object replyUserAvatar;
    private int commentUserId;
    private int objectType;
    private Object replyUserNickname;
    private List<TalksBean.CommentBean.ChildCommentsBean> childComments;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Object getParentId() {
        return parentId;
    }

    public void setParentId(Object parentId) {
        this.parentId = parentId;
    }

    public Object getCommentUserAvatar() {
        return commentUserAvatar;
    }

    public void setCommentUserAvatar(Object commentUserAvatar) {
        this.commentUserAvatar = commentUserAvatar;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public Object getCommentUserNickname() {
        return commentUserNickname;
    }

    public void setCommentUserNickname(Object commentUserNickname) {
        this.commentUserNickname = commentUserNickname;
    }

    public Object getReplyUserId() {
        return replyUserId;
    }

    public void setReplyUserId(Object replyUserId) {
        this.replyUserId = replyUserId;
    }

    public Object getReplyUserAvatar() {
        return replyUserAvatar;
    }

    public void setReplyUserAvatar(Object replyUserAvatar) {
        this.replyUserAvatar = replyUserAvatar;
    }

    public int getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(int commentUserId) {
        this.commentUserId = commentUserId;
    }

    public int getObjectType() {
        return objectType;
    }

    public void setObjectType(int objectType) {
        this.objectType = objectType;
    }

    public Object getReplyUserNickname() {
        return replyUserNickname;
    }

    public void setReplyUserNickname(Object replyUserNickname) {
        this.replyUserNickname = replyUserNickname;
    }

    public List<TalksBean.CommentBean.ChildCommentsBean> getChildComments() {
        return childComments;
    }

    public void setChildComments(List<TalksBean.CommentBean.ChildCommentsBean> childComments) {
        this.childComments = childComments;
    }

}
