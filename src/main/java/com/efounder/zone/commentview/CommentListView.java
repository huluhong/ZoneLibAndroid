package com.efounder.zone.commentview;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.efounder.zone.R;
import com.efounder.zone.bean.TalksBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 显示评论列表的组件
 * Created by slp on 2018/5/9.
 */
public class CommentListView extends LinearLayout {

    private Context mContext;

    private View mView;
    private RecyclerView mCommentListView;

    private CommentListAdapter mCommentAdapter;

    private List<ZoneCommentBean> mCommentBeans;

    private onCommentClickListener mClickListener;

    public CommentListView(Context context) {
        this(context, null);
    }

    public CommentListView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CommentListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(VERTICAL);
        this.mContext = context;
        initView();
    }

    private void initView() {
        mCommentBeans = new ArrayList<>();
        mView = LayoutInflater.from(mContext).inflate(R.layout.mobile_zone_view_commentlist, null);
        addView(mView);
        mCommentListView = mView.findViewById(R.id.rv_comment_list);
        mCommentListView.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });

    }

    /**
     * 设置评论列表数据
     * 处理数据格式
     * @param commentBeanList 评论列表list
     */
    public void setCommentData(List<TalksBean.CommentBean> commentBeanList){
//        this.mCommentBeans = commentBeanList;
        mCommentBeans.clear();
        //处理数据结构
        for (TalksBean.CommentBean commentBean:commentBeanList){
            ZoneCommentBean zoneCommentBean = new ZoneCommentBean();

            zoneCommentBean.setId(commentBean.getId());
            zoneCommentBean.setCreatetime(commentBean.getCreatetime());
            zoneCommentBean.setContent(commentBean.getContent());
            zoneCommentBean.setParentId(commentBean.getParentId());
            zoneCommentBean.setChildComments(commentBean.getChildComments());
            zoneCommentBean.setCommentUserAvatar(commentBean.getCommentUserAvatar());
            zoneCommentBean.setObjectId(commentBean.getObjectId());
            zoneCommentBean.setCommentUserNickname(commentBean.getCommentUserNickname());
            zoneCommentBean.setReplyUserId( commentBean.getReplyUserId());
            zoneCommentBean.setReplyUserAvatar(commentBean.getReplyUserAvatar());
            zoneCommentBean.setCommentUserId(commentBean.getCommentUserId());
            zoneCommentBean.setObjectType(commentBean.getObjectType());
            zoneCommentBean.setReplyUserNickname(commentBean.getReplyUserNickname());

            mCommentBeans.add(zoneCommentBean);

            if (commentBean.getChildComments()!= null){
                for (TalksBean.CommentBean.ChildCommentsBean childCommentsBean:commentBean.getChildComments()){
                    ZoneCommentBean zoneCommentBean1 = new ZoneCommentBean();

                    zoneCommentBean1.setId(childCommentsBean.getId());
                    zoneCommentBean1.setCreatetime(childCommentsBean.getCreatetime());
                    zoneCommentBean1.setContent(childCommentsBean.getContent());
                    zoneCommentBean1.setParentId(childCommentsBean.getParentId());
                    zoneCommentBean1.setChildComments((List<TalksBean.CommentBean.ChildCommentsBean>) childCommentsBean.getChildComments());
                    zoneCommentBean1.setCommentUserAvatar(childCommentsBean.getCommentUserAvatar());
                    zoneCommentBean1.setObjectId(childCommentsBean.getObjectId());
                    zoneCommentBean1.setCommentUserNickname(childCommentsBean.getCommentUserNickname());
                    zoneCommentBean1.setReplyUserId(childCommentsBean.getReplyUserId());
                    zoneCommentBean1.setReplyUserAvatar(childCommentsBean.getReplyUserAvatar());
                    zoneCommentBean1.setCommentUserId(childCommentsBean.getCommentUserId());
                    zoneCommentBean1.setObjectType(childCommentsBean.getObjectType());
                    zoneCommentBean1.setReplyUserNickname(childCommentsBean.getReplyUserNickname());

                    mCommentBeans.add(zoneCommentBean1);
                }
            }
        }
        mCommentAdapter = new CommentListAdapter(mCommentBeans, mContext, new CommentListAdapter.onCommentListener() {
            @Override
            public void onUserClick(String userId) {
                if(mClickListener != null){
                    mClickListener.onUserClick(userId);
                }
            }

            @Override
            public void onCommentClick(ZoneCommentBean zoneCommentBean) {
                if(mClickListener != null){
                    mClickListener.onCommentClick(zoneCommentBean);
                }
            }
        });
        mCommentListView.setAdapter(mCommentAdapter);
    }

    /**
     * 设置点击监听
     * @param listener
     */
    public void setCommentClickListener(onCommentClickListener listener){
        this.mClickListener = listener;
    }


    /**
     * 通知数据变化
     * @param commentBeanList
     */
    public void notifyDataChanged(List<TalksBean.CommentBean> commentBeanList){
        mCommentBeans.clear();

        List<ZoneCommentBean> zoneCommentBeanList = new ArrayList<>();

        for (TalksBean.CommentBean commentBean:commentBeanList){
            ZoneCommentBean zoneCommentBean = new ZoneCommentBean();

            zoneCommentBean.setId(commentBean.getId());
            zoneCommentBean.setCreatetime(commentBean.getCreatetime());
            zoneCommentBean.setContent(commentBean.getContent());
            zoneCommentBean.setParentId(commentBean.getParentId());
            zoneCommentBean.setChildComments(commentBean.getChildComments());
            zoneCommentBean.setCommentUserAvatar(commentBean.getCommentUserAvatar());
            zoneCommentBean.setObjectId(commentBean.getObjectId());
            zoneCommentBean.setCommentUserNickname(commentBean.getCommentUserNickname());
            zoneCommentBean.setReplyUserId( commentBean.getReplyUserId());
            zoneCommentBean.setReplyUserAvatar(commentBean.getReplyUserAvatar());
            zoneCommentBean.setCommentUserId(commentBean.getCommentUserId());
            zoneCommentBean.setObjectType(commentBean.getObjectType());
            zoneCommentBean.setReplyUserNickname(commentBean.getReplyUserNickname());

            zoneCommentBeanList.add(zoneCommentBean);

            if (commentBean.getChildComments()!= null){
                for (TalksBean.CommentBean.ChildCommentsBean childCommentsBean:commentBean.getChildComments()){
                    ZoneCommentBean zoneCommentBean1 = new ZoneCommentBean();

                    zoneCommentBean1.setId(childCommentsBean.getId());
                    zoneCommentBean1.setCreatetime(childCommentsBean.getCreatetime());
                    zoneCommentBean1.setContent(childCommentsBean.getContent());
                    zoneCommentBean1.setParentId(childCommentsBean.getParentId());
                    zoneCommentBean1.setChildComments((List<TalksBean.CommentBean.ChildCommentsBean>) childCommentsBean.getChildComments());
                    zoneCommentBean1.setCommentUserAvatar(childCommentsBean.getCommentUserAvatar());
                    zoneCommentBean1.setObjectId(childCommentsBean.getObjectId());
                    zoneCommentBean1.setCommentUserNickname(childCommentsBean.getCommentUserNickname());
                    zoneCommentBean1.setReplyUserId(childCommentsBean.getReplyUserId());
                    zoneCommentBean1.setReplyUserAvatar(childCommentsBean.getReplyUserAvatar());
                    zoneCommentBean1.setCommentUserId(childCommentsBean.getCommentUserId());
                    zoneCommentBean1.setObjectType(childCommentsBean.getObjectType());
                    zoneCommentBean1.setReplyUserNickname(childCommentsBean.getReplyUserNickname());

                    zoneCommentBeanList.add(zoneCommentBean1);
                }
            }
        }
        mCommentBeans.addAll(zoneCommentBeanList);
        mCommentAdapter.notifyDataSetChanged();
    }

    public interface onCommentClickListener{
        /**
         * 点击评论人
         * @param userId 评论人Id
         */
        void onUserClick(String userId);

        /**
         * 点击评论内容
         * @param zoneCommentBean 评论的bean
         */
        void onCommentClick(ZoneCommentBean zoneCommentBean);
    }

}
