package com.efounder.zone.commentview;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.efounder.chat.activity.ViewPagerActivity;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;
import com.efounder.zone.widget.CircleMovementMethod;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.photopicker.widget.BGANinePhotoLayout;

/**
 * 动态详情界面中评论的子评论适配器
 * Created by slp on 2018/5/15.
 */
public class ChildCommentListAdapter extends RecyclerView.Adapter<ChildCommentListAdapter.CommentViewHolder> implements BGANinePhotoLayout.Delegate {

    private List<ZoneCommentBean> mCommentBeanList;
    private Context mContext;
    private WeChatDBManager mWeChatDBManager;
    private onCommentListener listener;

    public ChildCommentListAdapter(List<ZoneCommentBean> commentBeanList, Context context, onCommentListener listener) {
        this.mCommentBeanList = commentBeanList;
        this.mContext = context;
        this.mWeChatDBManager = WeChatDBManager.getInstance();
        this.listener = listener;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.mobile_zone_item_child_comment, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        ZoneCommentBean zoneCommentBean = mCommentBeanList.get(position);
        String content = zoneCommentBean.getContent();

        //判断content中是否为json格式
        try {
            JSONObject jsonObject = new JSONObject(content);

//            评论文字
            if (jsonObject.has("text")) {
                String commentText = jsonObject.getString("text");

                setCommentText(zoneCommentBean, holder.tvCommentDetail, commentText);
            }
//            评论图片
            if (jsonObject.has("images")) {
                JSONArray jsonArray = jsonObject.getJSONArray("images");
                ArrayList<String> imgList = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    imgList.add(jsonArray.getString(i));
                }
                if (imgList.size() <= 0) {
                    holder.bplCommentDetailPhoto.setVisibility(View.GONE);
                } else {
                    holder.bplCommentDetailPhoto.setVisibility(View.VISIBLE);
                    holder.bplCommentDetailPhoto.setData(imgList);
                    holder.bplCommentDetailPhoto.setDelegate(ChildCommentListAdapter.this);
                }
            }

        } catch (Exception e) {
//            不是json格式直接显示评论
            holder.bplCommentDetailPhoto.setVisibility(View.GONE);
            String commentText = content;
            setCommentText(zoneCommentBean, holder.tvCommentDetail, commentText);
        }
    }

    /**
     * 设置显示的内容
     *
     * @param commentBean
     * @param tv
     * @param commentText
     */
    private void setCommentText(final ZoneCommentBean commentBean, TextView tv, String commentText) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        String commentUserId = String.valueOf(commentBean.getCommentUserId());


        String commentUserName = null;
        String replayUserName =null;

        if (commentBean.getCommentUserNickname()!=null){
            commentUserName = (String) commentBean.getCommentUserNickname();
        }else{
            User commentUser = mWeChatDBManager.getOneUserById(commentBean.getCommentUserId());
            commentUserName= commentUser.getNickName();
        }

        if (commentBean.getReplyUserNickname()!=null){
            replayUserName = (String) commentBean.getReplyUserNickname();
        }else{
            User commentUser = mWeChatDBManager.getOneUserById((int)commentBean.getReplyUserId());
            replayUserName= commentUser.getNickName();
        }


        int replyId = (int) commentBean.getReplyUserId();
        String replyUserId = String.valueOf(replyId);
       // User replyUser = mWeChatDBManager.getOneUserById(replyId);

        builder.append(setClickableSpan(commentUserName, commentUserId));
        builder.append(ResStringUtil.getString(R.string.zone_reply));
        builder.append(setClickableSpan(replayUserName, replyUserId));
        builder.append(" : ");
        builder.append(setClickableSpanContent(commentText, commentBean));
        builder.append(" ");
        tv.setText(builder);
        // 设置点击背景色
        tv.setHighlightColor(mContext.getResources().getColor(android.R.color.transparent));

        final CircleMovementMethod method = new CircleMovementMethod(0x00000000, 0x00000000);
        tv.setMovementMethod(method);

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onCommentClick(commentBean);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mCommentBeanList.size();
    }

    /**
     * 设置评论名字的点击事件
     *
     * @param item
     * @param userId
     * @return
     */
    public SpannableString setClickableSpan(final String item, final String userId) {
        final SpannableString string = new SpannableString(item);
        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                if (listener != null) {
                    listener.onUserClick(userId);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                // 设置显示的用户名文本颜色
                ds.setColor(mContext.getResources().getColor(R.color.mobile_zone_like_comment_people_textcolor));
                ds.setUnderlineText(false);
            }
        };

        string.setSpan(span, 0, string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return string;
    }

    /**
     * 设置评论内容的点击事件
     *
     * @param item
     * @param commentBean
     * @return
     */
    public SpannableString setClickableSpanContent(final String item, final ZoneCommentBean commentBean) {
        final SpannableString string = new SpannableString(item);
        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
//                Toast.makeText(mContext, " content: " + item, Toast.LENGTH_SHORT).show();
                if (listener != null) {
                    listener.onCommentClick(commentBean);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                // 设置显示的内容文本颜色
                ds.setColor(mContext.getResources().getColor(R.color.black_text_color));
                ds.setUnderlineText(false);
            }
        };
        string.setSpan(span, 0, string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return string;
    }

    /**
     * 点击评论中的图片  进入详情页
     * @param ninePhotoLayout
     * @param view
     * @param position
     * @param model
     * @param models
     */
    @Override
    public void onClickNinePhotoItem(BGANinePhotoLayout ninePhotoLayout, View view, int position, String model, List<String> models) {
        Intent intent = new Intent(mContext, ViewPagerActivity.class);
        String[] strings = models.toArray(new String[0]);
        intent.putExtra("urls", strings);
        intent.putExtra("position", position);

        mContext.startActivity(intent);
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCommentDetail;
        private BGANinePhotoLayout bplCommentDetailPhoto;

        public CommentViewHolder(View itemView) {
            super(itemView);
            this.tvCommentDetail = (TextView) itemView.findViewById(R.id.tv_comment_detail);
            this.bplCommentDetailPhoto = (BGANinePhotoLayout) itemView.findViewById(R.id.bpl_comment_detail_photo);
        }
    }

    public interface onCommentListener {

        /**
         * 点击评论名字
         * @param userId 评论人的id
         */
        void onUserClick(String userId);

        /**
         * 点击评论文字
         * @param zoneCommentBean 评论的bean
         */
        void onCommentClick(ZoneCommentBean zoneCommentBean);
    }
}
