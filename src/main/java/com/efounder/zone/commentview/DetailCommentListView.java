package com.efounder.zone.commentview;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.efounder.zone.R;
import com.efounder.zone.bean.TalksBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 动态详情界面的评论组件
 * Created by slp on 2018/5/15.
 */
public class DetailCommentListView extends LinearLayout {

    private Context mContext;

    private View mView;
    private RecyclerView mCommentsView;

    private List<ZoneCommentBean> mCommentBeanList;

    private DetailCommentListAdapter mCommentListAdapter;

    private OnDetailCommentItemListener mDetailCommentItemListener;

    public DetailCommentListView(Context context) {
        this(context, null);
    }

    public DetailCommentListView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailCommentListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(VERTICAL);
        this.mContext = context;
        initView();
    }

    private void initView() {
        mCommentBeanList = new ArrayList<>();
        mView = LayoutInflater.from(mContext).inflate(R.layout.mobile_zone_view_detail_commentlist, null);
        addView(mView);
        mCommentsView = mView.findViewById(R.id.rv_detail_comment_list);
        mCommentsView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
    }


    /**
     * 设置评论数据
     * @param commentBeanList
     */
    public void setCommentData(List<TalksBean.CommentBean> commentBeanList) {
        this.mCommentBeanList.clear();

        for (TalksBean.CommentBean commentBean:commentBeanList) {
            ZoneCommentBean zoneCommentBean = new ZoneCommentBean();

            zoneCommentBean.setId(commentBean.getId());
            zoneCommentBean.setCreatetime(commentBean.getCreatetime());
            zoneCommentBean.setContent(commentBean.getContent());
            zoneCommentBean.setParentId(commentBean.getParentId());
            zoneCommentBean.setChildComments(commentBean.getChildComments());
            zoneCommentBean.setCommentUserAvatar(commentBean.getCommentUserAvatar());
            zoneCommentBean.setObjectId(commentBean.getObjectId());
            zoneCommentBean.setCommentUserNickname(commentBean.getCommentUserNickname());
            zoneCommentBean.setReplyUserId(commentBean.getReplyUserId());
            zoneCommentBean.setReplyUserAvatar(commentBean.getReplyUserAvatar());
            zoneCommentBean.setCommentUserId(commentBean.getCommentUserId());
            zoneCommentBean.setObjectType(commentBean.getObjectType());
            zoneCommentBean.setReplyUserNickname(commentBean.getReplyUserNickname());

            mCommentBeanList.add(zoneCommentBean);
        }

        mCommentListAdapter = new DetailCommentListAdapter(mCommentBeanList, mContext, new DetailCommentListAdapter.OnDetailCommentListener() {
            @Override
            public void onNameClick(String userId) {
                if(mDetailCommentItemListener != null){
                    mDetailCommentItemListener.onNameClick(userId);
                }
            }

            @Override
            public void onCommentClick(ZoneCommentBean commentBean) {
                if(mDetailCommentItemListener != null){
                    mDetailCommentItemListener.onCommentClick(commentBean);
                }
            }

        });

        mCommentsView.setAdapter(mCommentListAdapter);
    }

    /**
     * 设置点击监听
     * @param listener
     */
    public void setDetailCommentItemListener(OnDetailCommentItemListener listener){
        this.mDetailCommentItemListener = listener;
    }


    public interface OnDetailCommentItemListener{

        /**
         * 点击评论人
         * @param userId 评论人的id
         */
        void onNameClick(String userId);

        /**
         * 点击评论的内容
         * @param commentBean 评论的bean
         */
        void onCommentClick(ZoneCommentBean commentBean);

    }
}
