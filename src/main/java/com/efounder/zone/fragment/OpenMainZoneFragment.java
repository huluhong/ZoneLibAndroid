package com.efounder.zone.fragment;

import android.content.Intent;
import android.view.View;

import com.core.xml.StubObject;
import com.efounder.chat.event.RightTopMenuClickEvent;
import com.efounder.chat.event.TabReselectedEvent;
import com.efounder.model.UpdatePageSlideTitleEvent;
import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;
import com.efounder.zone.activity.SendPostActivity;
import com.efounder.zone.activity.ZoneBlackListActivity;
import com.efounder.zone.bean.ZoneAddPopWindowBean;
import com.efounder.zone.widget.CommonZonePopWindow;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author yqs
 * 空间状态主页面（星际通讯专用）
 */
public class OpenMainZoneFragment extends MainZoneFragment {

    private static final String TAG = "OpenMainZoneFragment";
    private CommonZonePopWindow dropDownPop;

    public OpenMainZoneFragment() {
        super();
    }

    @Override
    protected void initCommonPopView() {
        commonOtherPopWindow = CommonZonePopWindow.getInstance(getActivity());
        commonOtherPopWindow.setItemOnClickListener(new CommonZonePopWindow.PopWindowItemOnClickListener() {
            @Override
            public void onClick(View view, int position, ZoneAddPopWindowBean bean) {
                String currentTime = String.valueOf(System.currentTimeMillis());
                if (position == 0) {
                    Intent intent = new Intent(getParentFragment().getContext(), SendPostActivity.class);
                    startActivityForResult(intent, MainZoneFragment.REQUEST_SEND_POST);

                } else if (position == 1) {
                    isRefresh = true;
                    dataOrigin = "getEventListFilterBlackList";
                    refreshLayout.autoRefresh();
                    if (isSelf) {
                        tvTitle.setText(R.string.zone_gravitational_all);
                        StubObject stubObject = (StubObject) getArguments().getSerializable("stubObject");
                        EventBus.getDefault().post(new UpdatePageSlideTitleEvent(ResStringUtil.getString(R.string.zone_gravitational_all), stubObject));
                    }

                    // getTalksData(currentTime);
                } else if (position == 2) {
                    isRefresh = true;
                    dataOrigin = "friends";
                    refreshLayout.autoRefresh();
                    // getTalksData(currentTime);
                    if (isSelf) {
                        tvTitle.setText(R.string.zone_gravitational_friend);
                        StubObject stubObject = (StubObject) getArguments().getSerializable("stubObject");
                        EventBus.getDefault().post(new UpdatePageSlideTitleEvent(ResStringUtil.getString(R.string.zone_gravitational_friend), stubObject));
                    }
                } else if (position == 3) {
                    isRefresh = true;
                    dataOrigin = "getEventListStar";
                    refreshLayout.autoRefresh();
                    if (isSelf) {
                        tvTitle.setText(R.string.zone_gravitational_follow);
                        StubObject stubObject = (StubObject) getArguments().getSerializable("stubObject");
                        EventBus.getDefault().post(new UpdatePageSlideTitleEvent(ResStringUtil.getString(R.string.zone_gravitational_follow), stubObject));
                    }
                    //getTalksData(currentTime);
                } else if (position == 4) {
                    //黑名单
                    ZoneBlackListActivity.start(getActivity());
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (commonOtherPopWindow != null && commonOtherPopWindow.isShowing()) {
            commonOtherPopWindow.dismiss();
        }
        if (dropDownPop != null && dropDownPop.isShowing()) {
            dropDownPop.dismiss();
        }
    }

    /**
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showPopMenu(TabReselectedEvent event) {
        StubObject stubObject = event.getStubObject();
        if (stubObject.getString("AndroidShow", "").equals("com.efounder.zone.fragment.OpenMainZoneFragment")) {
            commonOtherPopWindow.hideRightArrow();
            //不显示发射以及黑名单
            commonOtherPopWindow.conentView.getChildAt(1).setVisibility(View.GONE);
            commonOtherPopWindow.conentView.getChildAt(5).setVisibility(View.GONE);
            if (!getActivity().isFinishing()) {
                commonOtherPopWindow.showPopupWindow(event.getView());
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showPopMenu1(RightTopMenuClickEvent event) {
        StubObject stubObject = event.getStubObject();
        if (stubObject.getString("AndroidShow", "").equals("com.efounder.zone.fragment.OpenMainZoneFragment")) {
            dropDownPop = CommonZonePopWindow.getInstance(getActivity());

            dropDownPop.conentView.getChildAt(2).setVisibility(View.GONE);
            dropDownPop.conentView.getChildAt(3).setVisibility(View.GONE);
            dropDownPop.conentView.getChildAt(4).setVisibility(View.GONE);
            dropDownPop.setItemOnClickListener(new CommonZonePopWindow.PopWindowItemOnClickListener() {
                @Override
                public void onClick(View view, int position, ZoneAddPopWindowBean bean) {
                    if (position == 0) {
                        //发送新动态
                        Intent intent = new Intent(getParentFragment().getContext(), SendPostActivity.class);
                        startActivityForResult(intent, MainZoneFragment.REQUEST_SEND_POST);
                    } else if (position == 4) {
                        //去黑名单
                        ZoneBlackListActivity.start(getActivity());
                    }
                    if (dropDownPop != null && dropDownPop.isShowing()) {
                        dropDownPop.dismiss();
                    }
                }
            });
            if (!getActivity().isFinishing()) {
                dropDownPop.showPopupWindow(event.getView());
            }
        }
    }
}
