package com.efounder.zone.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.utils.ResStringUtil;
import com.efounder.zone.R;
import com.efounder.zone.bean.TalksBean;
import com.efounder.zone.event.MobileZoneTransmitContentEvent;
import com.efounder.zone.util.CreateBeanUtil;
import com.utilcode.util.DeviceUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * @author will
 *         评论输入组件
 */
public class TransmitDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = "TransmitDialogFragment";
    private EditText etTransmitReason;
    private Button btnSend;
    private TalksBean talksBean;
    private ArrayList<String> images = new ArrayList<>();

    public TransmitDialogFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setGravity(Gravity.BOTTOM);
        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);//支持输入法
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mobile_zone_comment_input, container, false);
        initView(view);
        return view;
    }

    public void setParams(Context context, TalksBean talksBean) {
        this.talksBean = talksBean;
    }

    private void initView(View view) {
        ImageView ivAt, ivEmoji, ivPhoto;
//        ivAt = view.findViewById(R.id.iv_at);
//        ivAt.setOnClickListener(this);
//        ivEmoji = view.findViewById(R.id.iv_emoji);
//        ivEmoji.setOnClickListener(this);
        ivPhoto = view.findViewById(R.id.iv_photo);
        ivPhoto.setVisibility(View.GONE);
        etTransmitReason = view.findViewById(R.id.et_comment);
        etTransmitReason.setHint(ResStringUtil.getString(R.string.zone_forward_reason));
        etTransmitReason.setFocusable(true);
        btnSend = view.findViewById(R.id.button_send);
        btnSend.setOnClickListener(this);
        //隐藏图片评论view
        LinearLayout llCommentImage = view.findViewById(R.id.ll_comment_photo);
        llCommentImage.setVisibility(View.GONE);
        etTransmitReason.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND &&
                        btnSend.isEnabled()) {
                    String content = etTransmitReason.getText().toString();
                    sendComment(content);
                    dismiss();
                    handled = true;
                }
                return handled;
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.button_send) {
            String content = etTransmitReason.getText().toString();
            sendComment(content);
            dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 转发
     *
     * @param content 转发原因
     */
    private void sendComment(String content) {
        if ("".equals(content)) {
            content = ResStringUtil.getString(R.string.zone_repost);
        }
        talksBean = new CreateBeanUtil().createTransmitTalksBean(talksBean,DeviceUtils.getModel(),content);
        EventBus.getDefault().post(new MobileZoneTransmitContentEvent(talksBean));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
}
