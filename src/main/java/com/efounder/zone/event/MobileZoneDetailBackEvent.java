package com.efounder.zone.event;

import com.efounder.zone.bean.TalksBean;

/**
 * Created by zsy on 2018/5/114.
 * 说说详情返回主页面，刷新item
 */
public class MobileZoneDetailBackEvent {

    private TalksBean mTalksBean;
    private int mPosition;

    public MobileZoneDetailBackEvent(TalksBean talksBean, int position) {
        mTalksBean = talksBean;
        mPosition = position;
    }

    public TalksBean getTalksBean() {
        return mTalksBean;
    }

    public int getPosition() {
        return mPosition;
    }
}
