package com.efounder.zone.event;

import com.efounder.zone.bean.TalksBean;

/**
 * Created by zsy on 2018/5/114.
 * 转发说说的事件
 */
public class MobileZoneTransmitContentEvent {

    private TalksBean mTalksBean;

    public MobileZoneTransmitContentEvent(TalksBean talksBean) {
        mTalksBean = talksBean;
    }

    public TalksBean getTalksBean() {
        return mTalksBean;
    }
}
