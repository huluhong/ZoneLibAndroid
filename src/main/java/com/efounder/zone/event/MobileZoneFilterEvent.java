package com.efounder.zone.event;

/**
 * 过滤数据源
 */
public class MobileZoneFilterEvent {
    private int userId;

    //type =1 黑名单
    private  int type;

    public MobileZoneFilterEvent(int userId, int type) {
        this.userId = userId;
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public MobileZoneFilterEvent setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getType() {
        return type;
    }

    public MobileZoneFilterEvent setType(int type) {
        this.type = type;
        return this;
    }
}
