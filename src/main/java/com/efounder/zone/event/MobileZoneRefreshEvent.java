package com.efounder.zone.event;

import com.efounder.zone.bean.TalksBean;

/**
 * Created by zsy on 2018/5/114.
 * 说说详情页面经过点赞，评论等操作后，刷新详情界面
 */
public class MobileZoneRefreshEvent {

    private final String mStatus;
    private TalksBean mTalksBean;

    public MobileZoneRefreshEvent(TalksBean talksBean, String status) {
        mTalksBean = talksBean;
        mStatus = status;
    }

    public TalksBean getTalksBean() {
        return mTalksBean;
    }

    public String getmStatus(){
        return mStatus;
    }

}
