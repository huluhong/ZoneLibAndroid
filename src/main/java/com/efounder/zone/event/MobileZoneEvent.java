package com.efounder.zone.event;

public class MobileZoneEvent {
    private int position;

    public MobileZoneEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
