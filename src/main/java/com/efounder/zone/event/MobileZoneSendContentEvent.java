package com.efounder.zone.event;

import com.efounder.zone.bean.TalksBean;

/**
 * Created by slp on 2018/5/11.
 */
public class MobileZoneSendContentEvent {

    private TalksBean mTalksBean;

    public MobileZoneSendContentEvent(TalksBean talksBean) {
        mTalksBean = talksBean;
    }

    public TalksBean getTalksBean() {
        return mTalksBean;
    }
}
