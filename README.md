
### 项目介绍
Android空间的库工程。

### 功能介绍
- 发表动态，支持文字、图片、小视频等
- 查看动态
- 点赞、评论等互动

### 版权声明
本软件使用 GPL3.0 协议，请严格遵照协议内容!

### 合作及联系
- 联系QQ群: 976048137, 977276269
- 联系邮箱：app@turbochain.ai

<img src="./assets/qq_ipcomm.jpg" width="300" />
<img src="./assets/qq_ipassets.jpg" width="300" />
